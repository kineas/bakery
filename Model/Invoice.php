<?php
App::uses('AppModel', 'Model');
/**
 * Invoice Model
 *
 */
class Invoice extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'invoice';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'InvoiceId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'InvoiceDate';
	
		public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'CustomerId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
