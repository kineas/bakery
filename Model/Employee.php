<?php
App::uses('AppModel', 'Model');
/**
 * Employee Model
 *
 */
class Employee extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'employee';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'EmployeeId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'LastName';

}
