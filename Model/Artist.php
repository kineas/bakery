<?php
App::uses('AppModel', 'Model');
/**
 * Artist Model
 *
 */
class Artist extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'artist';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'ArtistId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'Name';

}
