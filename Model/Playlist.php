<?php
App::uses('AppModel', 'Model');
/**
 * Playlist Model
 *
 */
class Playlist extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'playlist';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'PlaylistId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'Name';

}
