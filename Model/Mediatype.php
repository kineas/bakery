<?php
App::uses('AppModel', 'Model');
/**
 * Mediatype Model
 *
 */
class Mediatype extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'mediatype';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'MediaTypeId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'Name';

}
