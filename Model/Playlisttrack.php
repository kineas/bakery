<?php
App::uses('AppModel', 'Model');
/**
 * Playlisttrack Model
 *
 */
class Playlisttrack extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'playlisttrack';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'PlaylistId';

	public $belongsTo = array(
		'Track' => array(
			'className' => 'Track',
			'foreignKey' => 'TrackId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
