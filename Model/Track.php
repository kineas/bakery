<?php
App::uses('AppModel', 'Model');
/**
 * Track Model
 *
 */
class Track extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'track';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'TrackId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'Name';
	
	public $belongsTo = array(
		'MediaType' => array(
			'className' => 'MediaType',
			'foreignKey' => 'MediaTypeId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Album' => array(
			'className' => 'Album',
			'foreignKey' => 'AlbumId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Genre' => array(
			'className' => 'Genre',
			'foreignKey' => 'GenreId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
		
	);

}
