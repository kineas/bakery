<?php
App::uses('AppModel', 'Model');
/**
 * Album Model
 *
 */
class Album extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'album';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'AlbumId';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'Title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ArtistId' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	public $belongsTo = array(
		'Artist' => array(
			'className' => 'Artist',
			'foreignKey' => 'ArtistId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
