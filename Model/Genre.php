<?php
App::uses('AppModel', 'Model');
/**
 * Genre Model
 *
 */
class Genre extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'genre';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'GenreId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'Name';

}
