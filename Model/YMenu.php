<?php
App::uses('AppModel', 'Model');
/**
 * YMenu Model
 *
 * @property Acos $Acos
 * @property YMenu $ParentMenu
 */
class YMenu extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		
		'YMenus' => array(
			'className' => 'YMenu',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
