<?php
App::uses('AppModel', 'Model');
/**
 * Invoiceline Model
 *
 */
class Invoiceline extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'invoiceline';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'InvoiceLineId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'UnitPrice';
	
	public $belongsTo = array(
		'Invoice' => array(
			'className' => 'Invoice',
			'foreignKey' => 'InvoiceId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Track' => array(
			'className' => 'Track',
			'foreignKey' => 'TrackId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
