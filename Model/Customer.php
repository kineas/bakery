<?php
App::uses('AppModel', 'Model');
/**
 * Customer Model
 *
 */
class Customer extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'customer';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'CustomerId';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'FirstName';
	
	public $belongsTo = array(
		'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'SupportRepId',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
	);

}
