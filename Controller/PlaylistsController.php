<?php
App::uses('AppController', 'Controller');
/**
 * Playlists Controller
 *
 * @property Playlist $Playlist
 * @property PaginatorComponent $Paginator
 */
class PlaylistsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('playlists', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Playlist->recursive = 0;
		$this->set('playlists',  $this->Playlist->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Playlist->exists($id)) {
			throw new NotFoundException(__('Invalid playlist'));
		}
		$options = array('conditions' => array('Playlist.' . $this->Playlist->primaryKey => $id));
		$this->set('playlist', $this->Playlist->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Playlist->create();
			if ($this->Playlist->save(json_decode($this->request->query["_records"]))) {
				return $this->flash(__('The playlist has been saved.'), array('action' => 'index'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Playlist->exists($id)) {
			throw new NotFoundException(__('Invalid playlist'));
		}
		if ($this->request->query['_records']) {
			if ($this->Playlist->save(json_decode($this->request->query['_records']))) {
				return $this->flash(__('The playlist has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('Playlist.' . $this->Playlist->primaryKey => $id));
			$this->request->data = $this->Playlist->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Playlist->id = $id;
		if (!$this->Playlist->exists()) {
			throw new NotFoundException(__('Invalid playlist'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Playlist->delete()) {
			return $this->flash(__('The playlist has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The playlist could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}
