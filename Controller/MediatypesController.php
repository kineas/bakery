<?php
App::uses('AppController', 'Controller');
/**
 * Mediatypes Controller
 *
 * @property Mediatype $Mediatype
 * @property PaginatorComponent $Paginator
 */
class MediatypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('mediatypes', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Mediatype->recursive = 0;
		$this->set('mediatypes',  $this->Mediatype->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Mediatype->exists($id)) {
			throw new NotFoundException(__('Invalid mediatype'));
		}
		$options = array('conditions' => array('Mediatype.' . $this->Mediatype->primaryKey => $id));
		$this->set('mediatype', $this->Mediatype->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Mediatype->create();
			if ($this->Mediatype->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The mediatype has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('Mediatype.' . $this->Mediatype->primaryKey => $this->Mediatype->getLastInsertId()));
				$this->set('mediatype', $this->Mediatype->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The mediatype could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Mediatype->exists($id)) {
			throw new NotFoundException(__('Invalid mediatype'));
		}
		if ($this->request->query['_records']) {
			if ($this->Mediatype->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('Mediatype.' . $this->Mediatype->primaryKey => $id));
				$this->set('mediatype', $this->Mediatype->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The mediatype has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mediatype could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('Mediatype.' . $this->Mediatype->primaryKey => $id));
			$this->request->data = $this->Mediatype->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Mediatype->id = $id;
		if (!$this->Mediatype->exists()) {
			throw new NotFoundException(__('Invalid mediatype'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Mediatype->delete()) {
			$this->Session->setFlash(__('The mediatype has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mediatype could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
