<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	var $components = array(
    'FilterResults.Filter' => array(
        'auto' => array(
            'paginate' => false,
            'explode'  => true,  // recommended
        ),
        'explode' => array(
            'character'   => ' ',
            'concatenate' => 'AND',
        )
    ),
	'Acl',
	'Auth' => array(
					'authorize' => array(
							'Actions' => array('actionPath' => 'controllers')
					)
			),
	'Session',
);





var $helpers = array('Html', 'Form','Session',
    'FilterResults.Search' => array(
        'operators' => array(
            'LIKE'       => 'containing',
            'NOT LIKE'   => 'not containing',
            'LIKE BEGIN' => 'starting with',
            'LIKE END'   => 'ending with',
            '='  => 'equal to',
            '!=' => 'different',
            '>'  => 'greater than',
            '>=' => 'greater or equal to',
            '<'  => 'less than',
            '<=' => 'less or equal to'
        )
    )
);

 public function beforeFilter() {
        //Configure AuthComponent
        $this->Auth->loginAction = array(
            'plugin' => false, 
            'controller' => 'users',
            'action' => 'login'
            );
        $this->Auth->logoutRedirect = array(
            'plugin' => false, 
            'controller' => 'users',
            'action' => 'login'
            );
        $this->Auth->loginRedirect = '/';

        $this->Auth->authError = __('You are not authorized to access that location.');
		
        // If YALP not loaded then use Form Auth
        if (CakePlugin::loaded('YALP'))
            $this->Auth->authenticate = array('YALP.LDAP' => null);
		else
			$this->Session->setFlash( "LDAP plugin not enabled");

        parent::beforeFilter();
    }
}
