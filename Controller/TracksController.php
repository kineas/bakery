<?php
App::uses('AppController', 'Controller');
/**
 * Tracks Controller
 *
 * @property Track $Track
 * @property PaginatorComponent $Paginator
 */
class TracksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('tracks', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Track->recursive = 0;
		$this->set('tracks',  $this->Track->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Track->exists($id)) {
			throw new NotFoundException(__('Invalid track'));
		}
		$options = array('conditions' => array('Track.' . $this->Track->primaryKey => $id));
		$this->set('track', $this->Track->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Track->create();
			if ($this->Track->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The track has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('Track.' . $this->Track->primaryKey => $this->Track->getLastInsertId()));
				$this->set('track', $this->Track->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The track could not be saved. Please, try again.'));
			}
		}
		$mediaTypes = $this->Track->MediaType->find('list');
		$albums = $this->Track->Album->find('list');
		$genres = $this->Track->Genre->find('list');
		$this->set(compact('mediaTypes', 'albums', 'genres'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Track->exists($id)) {
			throw new NotFoundException(__('Invalid track'));
		}
		if ($this->request->query['_records']) {
			if ($this->Track->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('Track.' . $this->Track->primaryKey => $id));
				$this->set('track', $this->Track->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The track has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The track could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('Track.' . $this->Track->primaryKey => $id));
			$this->request->data = $this->Track->find('first', $options);
		}
		$mediaTypes = $this->Track->MediaType->find('list');
		$albums = $this->Track->Album->find('list');
		$genres = $this->Track->Genre->find('list');
		$this->set(compact('mediaTypes', 'albums', 'genres'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Track->id = $id;
		if (!$this->Track->exists()) {
			throw new NotFoundException(__('Invalid track'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Track->delete()) {
			$this->Session->setFlash(__('The track has been deleted.'));
		} else {
			$this->Session->setFlash(__('The track could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
