<?php
App::uses('AppController', 'Controller');
/**
 * ArosAcos Controller
 *
 * @property ArosAco $ArosAco
 * @property PaginatorComponent $Paginator
 */
class ArosAcosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('arosAcos', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->ArosAco->recursive = 0;
		$this->set('arosAcos',  $this->ArosAco->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->ArosAco->exists($id)) {
			throw new NotFoundException(__('Invalid aros aco'));
		}
		$options = array('conditions' => array('ArosAco.' . $this->ArosAco->primaryKey => $id));
		$this->set('arosAco', $this->ArosAco->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->ArosAco->create();
			if ($this->ArosAco->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The aros aco has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('ArosAco.' . $this->ArosAco->primaryKey => $this->ArosAco->getLastInsertId()));
				$this->set('arosAco', $this->ArosAco->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The aros aco could not be saved. Please, try again.'));
			}
		}
		$aros = $this->ArosAco->Aro->find('list');
		$acos = $this->ArosAco->Aco->find('list');
		$this->set(compact('aros', 'acos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->ArosAco->exists($id)) {
			throw new NotFoundException(__('Invalid aros aco'));
		}
		if ($this->request->query['_records']) {
			if ($this->ArosAco->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('ArosAco.' . $this->ArosAco->primaryKey => $id));
				$this->set('arosAco', $this->ArosAco->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The aros aco has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The aros aco could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('ArosAco.' . $this->ArosAco->primaryKey => $id));
			$this->request->data = $this->ArosAco->find('first', $options);
		}
		$aros = $this->ArosAco->Aro->find('list');
		$acos = $this->ArosAco->Aco->find('list');
		$this->set(compact('aros', 'acos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->ArosAco->id = $id;
		if (!$this->ArosAco->exists()) {
			throw new NotFoundException(__('Invalid aros aco'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->ArosAco->delete()) {
			$this->Session->setFlash(__('The aros aco has been deleted.'));
		} else {
			$this->Session->setFlash(__('The aros aco could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
