<?php
App::uses('AppController', 'Controller');
/**
 * Playlisttracks Controller
 *
 * @property Playlisttrack $Playlisttrack
 * @property PaginatorComponent $Paginator
 */
class PlaylisttracksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('playlisttracks', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Playlisttrack->recursive = 0;
		$this->set('playlisttracks',  $this->Playlisttrack->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Playlisttrack->exists($id)) {
			throw new NotFoundException(__('Invalid playlisttrack'));
		}
		$options = array('conditions' => array('Playlisttrack.' . $this->Playlisttrack->primaryKey => $id));
		$this->set('playlisttrack', $this->Playlisttrack->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Playlisttrack->create();
			if ($this->Playlisttrack->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The playlisttrack has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('Playlisttrack.' . $this->Playlisttrack->primaryKey => $this->Playlisttrack->getLastInsertId()));
				$this->set('playlisttrack', $this->Playlisttrack->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The playlisttrack could not be saved. Please, try again.'));
			}
		}
		$tracks = $this->Playlisttrack->Track->find('list');
		$this->set(compact('tracks'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Playlisttrack->exists($id)) {
			throw new NotFoundException(__('Invalid playlisttrack'));
		}
		if ($this->request->query['_records']) {
			if ($this->Playlisttrack->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('Playlisttrack.' . $this->Playlisttrack->primaryKey => $id));
				$this->set('playlisttrack', $this->Playlisttrack->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The playlisttrack has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The playlisttrack could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('Playlisttrack.' . $this->Playlisttrack->primaryKey => $id));
			$this->request->data = $this->Playlisttrack->find('first', $options);
		}
		$tracks = $this->Playlisttrack->Track->find('list');
		$this->set(compact('tracks'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Playlisttrack->id = $id;
		if (!$this->Playlisttrack->exists()) {
			throw new NotFoundException(__('Invalid playlisttrack'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Playlisttrack->delete()) {
			$this->Session->setFlash(__('The playlisttrack has been deleted.'));
		} else {
			$this->Session->setFlash(__('The playlisttrack could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
