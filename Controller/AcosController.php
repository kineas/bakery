<?php
App::uses('AppController', 'Controller');
/**
 * Acos Controller
 *
 * @property Aco $Aco
 * @property PaginatorComponent $Paginator
 */
class AcosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('acos', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Aco->recursive = 0;
		$this->set('acos',  $this->Aco->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Aco->exists($id)) {
			throw new NotFoundException(__('Invalid aco'));
		}
		$options = array('conditions' => array('Aco.' . $this->Aco->primaryKey => $id));
		$this->set('aco', $this->Aco->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Aco->create();
			if ($this->Aco->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The aco has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('Aco.' . $this->Aco->primaryKey => $this->Aco->getLastInsertId()));
				$this->set('aco', $this->Aco->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The aco could not be saved. Please, try again.'));
			}
		}
		$aros = $this->Aco->Aro->find('list');
		$this->set(compact('aros'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Aco->exists($id)) {
			throw new NotFoundException(__('Invalid aco'));
		}
		if ($this->request->query['_records']) {
			if ($this->Aco->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('Aco.' . $this->Aco->primaryKey => $id));
				$this->set('aco', $this->Aco->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The aco has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The aco could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('Aco.' . $this->Aco->primaryKey => $id));
			$this->request->data = $this->Aco->find('first', $options);
		}
		$aros = $this->Aco->Aro->find('list');
		$this->set(compact('aros'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Aco->id = $id;
		if (!$this->Aco->exists()) {
			throw new NotFoundException(__('Invalid aco'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Aco->delete()) {
			$this->Session->setFlash(__('The aco has been deleted.'));
		} else {
			$this->Session->setFlash(__('The aco could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
