<?php
App::uses('AppController', 'Controller');
/**
 * Artists Controller
 *
 * @property Artist $Artist
 * @property PaginatorComponent $Paginator
 */
class ArtistsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('artists', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Artist->recursive = 0;
		$this->set('artists',  $this->Artist->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Artist->exists($id)) {
			throw new NotFoundException(__('Invalid artist'));
		}
		$options = array('conditions' => array('Artist.' . $this->Artist->primaryKey => $id));
		$this->set('artist', $this->Artist->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Artist->create();
			if ($this->Artist->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The artist has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('Artist.' . $this->Artist->primaryKey => $this->Artist->getLastInsertId()));
				$this->set('artist', $this->Artist->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The artist could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Artist->exists($id)) {
			throw new NotFoundException(__('Invalid artist'));
		}
		if ($this->request->query['_records']) {
			if ($this->Artist->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('Artist.' . $this->Artist->primaryKey => $id));
				$this->set('artist', $this->Artist->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The artist has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The artist could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('Artist.' . $this->Artist->primaryKey => $id));
			$this->request->data = $this->Artist->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Artist->id = $id;
		if (!$this->Artist->exists()) {
			throw new NotFoundException(__('Invalid artist'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Artist->delete()) {
			$this->Session->setFlash(__('The artist has been deleted.'));
		} else {
			$this->Session->setFlash(__('The artist could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
