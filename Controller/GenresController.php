<?php
App::uses('AppController', 'Controller');
/**
 * Genres Controller
 *
 * @property Genre $Genre
 * @property PaginatorComponent $Paginator
 */
class GenresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('genres', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Genre->recursive = 0;
		$this->set('genres',  $this->Genre->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Genre->exists($id)) {
			throw new NotFoundException(__('Invalid genre'));
		}
		$options = array('conditions' => array('Genre.' . $this->Genre->primaryKey => $id));
		$this->set('genre', $this->Genre->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Genre->create();
			if ($this->Genre->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The genre has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('Genre.' . $this->Genre->primaryKey => $this->Genre->getLastInsertId()));
				$this->set('genre', $this->Genre->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The genre could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Genre->exists($id)) {
			throw new NotFoundException(__('Invalid genre'));
		}
		if ($this->request->query['_records']) {
			if ($this->Genre->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('Genre.' . $this->Genre->primaryKey => $id));
				$this->set('genre', $this->Genre->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The genre has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The genre could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('Genre.' . $this->Genre->primaryKey => $id));
			$this->request->data = $this->Genre->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Genre->id = $id;
		if (!$this->Genre->exists()) {
			throw new NotFoundException(__('Invalid genre'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Genre->delete()) {
			$this->Session->setFlash(__('The genre has been deleted.'));
		} else {
			$this->Session->setFlash(__('The genre could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
