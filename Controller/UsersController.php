<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}

	public function extdata() {
		$this->autoLayout = false; 
			
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('users', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->User->recursive = 0;
		$this->set('users',  $this->Album->find('all'));
	}	
	
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				//$this->Session->setFlash(__('The user has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('User.' . $this->User->primaryKey => $this->User->getLastInsertId()));
				$this->set('user', $this->User->find('first', $options));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false;
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->query['_records']) {
			if ($this->User->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
				$this->set('user', $this->User->find('first', $options));
				return true;
				// $this->Session->setFlash(__('The user has been saved.'));
				// return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index')); 
	}

/**
 * Login method
 *
 * @return void
 */
	public function login() {
		$this->layout = 'login';
		
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirect());
			}
			$this->Session->setFlash(__('Your username or password was incorrect.'));
		}

		if ($this->Session->read('Auth.User')) {
			$this->Session->setFlash(__('You are logged in!'));
			return $this->redirect('/');
		}
	}

/**
 * Logout method
 *
 * @return void
 */
	public function logout() {
		$this->Session->setFlash(__('Good-bye'));
		$this->redirect($this->Auth->logout());
	}

} // End of class
