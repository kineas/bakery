<?php
App::uses('AppController', 'Controller');
/**
 * Aros Controller
 *
 * @property Aro $Aro
 * @property PaginatorComponent $Paginator
 */
class ArosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('aros', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Aro->recursive = 0;
		$this->set('aros',  $this->Aro->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Aro->exists($id)) {
			throw new NotFoundException(__('Invalid aro'));
		}
		$options = array('conditions' => array('Aro.' . $this->Aro->primaryKey => $id));
		$this->set('aro', $this->Aro->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Aro->create();
			if ($this->Aro->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The aro has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('Aro.' . $this->Aro->primaryKey => $this->Aro->getLastInsertId()));
				$this->set('aro', $this->Aro->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The aro could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Aro->exists($id)) {
			throw new NotFoundException(__('Invalid aro'));
		}
		if ($this->request->query['_records']) {
			if ($this->Aro->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('Aro.' . $this->Aro->primaryKey => $id));
				$this->set('aro', $this->Aro->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The aro has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The aro could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('Aro.' . $this->Aro->primaryKey => $id));
			$this->request->data = $this->Aro->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Aro->id = $id;
		if (!$this->Aro->exists()) {
			throw new NotFoundException(__('Invalid aro'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Aro->delete()) {
			$this->Session->setFlash(__('The aro has been deleted.'));
		} else {
			$this->Session->setFlash(__('The aro could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
