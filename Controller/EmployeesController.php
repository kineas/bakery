<?php
App::uses('AppController', 'Controller');
/**
 * Employees Controller
 *
 * @property Employee $Employee
 * @property PaginatorComponent $Paginator
 */
class EmployeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}



	
	public function extdata() {
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());

   
		$this->set('employees', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->Employee->recursive = 0;
		$this->set('employees',  $this->Employee->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->Employee->exists($id)) {
			throw new NotFoundException(__('Invalid employee'));
		}
		$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
		$this->set('employee', $this->Employee->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->Employee->create();
			if ($this->Employee->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The employee has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $this->Employee->getLastInsertId()));
				$this->set('employee', $this->Employee->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The employee could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->Employee->exists($id)) {
			throw new NotFoundException(__('Invalid employee'));
		}
		if ($this->request->query['_records']) {
			if ($this->Employee->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
				$this->set('employee', $this->Employee->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The employee has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The employee could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
			$this->request->data = $this->Employee->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->Employee->id = $id;
		if (!$this->Employee->exists()) {
			throw new NotFoundException(__('Invalid employee'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Employee->delete()) {
			$this->Session->setFlash(__('The employee has been deleted.'));
		} else {
			$this->Session->setFlash(__('The employee could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
