<?php
App::uses('AppController', 'Controller');
/**
 * YMenus Controller
 *
 * @property YMenu $YMenu
 * @property PaginatorComponent $Paginator
 */
class YMenusController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
	}


	public function getmenu(){
		$this->autoLayout = false; 
		$this->YMenu->recursive = -1;
		$ymenu = $this->YMenu->find('all');
		
		
		$this->loadModel('Acos');
		$acos = $this->Acos->find('all');
		$menu = array();
		foreach($acos as $aco){
			//var_dump($aco);
			if($aco['Acos']['parent_id']=='1'){
				$found = $this->Acl->check(array(
					'model' => 'User',
					'foreign_key' => $_SESSION['Auth']['User']['id']
				), $aco['Acos']['alias']);
				if($found){
					foreach($ymenu as $ymen){
						if($ymen['YMenu']['acos_id']==$aco['Acos']['id']){
							$menu[] = $ymen;
						}
					}
				}
			}
		}
		//var_dump($menu);
		$this->set('yMenus', $menu);
    
	}
	
	public function extdata() {
		
		
		
		
		$this->autoLayout = false; 
		//$this->Track->recursive = 0;
		//$this->set('tracks', $this->Paginator->paginate());		
		$this->Filter->addFilters(
			array(
				'AND' => array('filter1','filter2','filter3','filter4','filter5','filter6','filter7','filter8','filter9','filter10','filter11','filter12','filter13','filter14','filter15','filter16','filter17','filter18','filter19','filter20')
			)
		);
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());
		
		$this->set('yMenus', $this->paginate());
	}
	
	public function extdataall() {
		$this->autoLayout = false; 
		$this->YMenu->recursive = -1;
		$this->set('yMenus',  $this->YMenu->find('all'));
	}	
	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->autoLayout = false;
		if (!$this->YMenu->exists($id)) {
			throw new NotFoundException(__('Invalid y menu'));
		}
		$options = array('conditions' => array('YMenu.' . $this->YMenu->primaryKey => $id));
		$this->set('yMenu', $this->YMenu->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->autoLayout = false;
		//if ($this->request->is('post')) 
		{
			$this->YMenu->create();
			if ($this->YMenu->save(json_decode($this->request->query["_records"]))) {
				//$this->Session->setFlash(__('The y menu has been saved.'));
				//return $this->redirect(array('action' => 'index'));
				$options = array('conditions' => array('YMenu.' . $this->YMenu->primaryKey => $this->YMenu->getLastInsertId()));
				$this->set('yMenu', $this->YMenu->find('first', $options));
				return true;
			} else {
				$this->Session->setFlash(__('The y menu could not be saved. Please, try again.'));
			}
		}
		$yMenus = $this->YMenu->YMenu->find('list');
		$this->set(compact('yMenus'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->autoLayout = false; 
		if (!$this->YMenu->exists($id)) {
			throw new NotFoundException(__('Invalid y menu'));
		}
		if ($this->request->query['_records']) {
			if ($this->YMenu->save(json_decode($this->request->query['_records']))) {
				$options = array('conditions' => array('YMenu.' . $this->YMenu->primaryKey => $id));
				$this->set('yMenu', $this->YMenu->find('first', $options));
				return true;
				//$this->Session->setFlash(__('The y menu has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The y menu could not be saved. Please, try again.'));
				return false;
			}
		} else {
			$options = array('conditions' => array('YMenu.' . $this->YMenu->primaryKey => $id));
			$this->request->data = $this->YMenu->find('first', $options);
		}
		$yMenus = $this->YMenu->YMenu->find('list');
		$this->set(compact('yMenus'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoLayout = false;
		$this->YMenu->id = $id;
		if (!$this->YMenu->exists()) {
			throw new NotFoundException(__('Invalid y menu'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->YMenu->delete()) {
			$this->Session->setFlash(__('The y menu has been deleted.'));
		} else {
			$this->Session->setFlash(__('The y menu could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
