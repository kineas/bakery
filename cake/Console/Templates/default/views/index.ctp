<?php
	//var_dump($singularVar);
	//var_dump($fields);
	//var_dump($associations["belongsTo"]);
	//var_dump($this->templateVars['schema']);
	
	$object = array();
	
	foreach($this->templateVars['schema'] as $key=>$val){
		//var_dump($val["key"]);
		if($val["key"] =="primary"){
			$object["primaryKey"]= $key;
			
		}
		
	}
	
	//var_dump($associations);
	
	$object["singular"] = $singularVar;
	$object["plural"] = $pluralVar;
	//
	foreach($fields as $field){
		$fi = array(
									"name"=>$field,
									"type"=>$this->templateVars['schema'][$field]['type'],								
									
								);
		foreach($associations["belongsTo"] as $k=>$v){	
		if(strtolower($v["foreignKey"])==strtolower($field)){
			$displayField =$v["displayField"];
			if($displayField==$v["primaryKey"]){
				foreach($v["fields"] as $kk=>$vv){
					if($displayField!=$vv && strpos($vv,'Id') == false)
						$displayField=$vv;
				}
				
			}
			$fi["belongsTo"] = array();
			$fi["belongsTo"]['model'] = $k;
			$fi["belongsTo"]["foreignKey"] = $v["primaryKey"];			
			$fi["belongsTo"]["displayField"] = $displayField;
			$fi["belongsTo"]["tableName"] = $k;
			$fi["belongsTo"]['fields'] = array();
			foreach($v["fields"] as $kk=>$vv){
					$fifi = array("name"=> $vv);
					if($vv==$fi["belongsTo"]["primaryKey"]){
						$fifi["type"]=$this->templateVars['schema'][$field]['type'];
						
					}
					else{
						$fifi["type"]="string";
					}
					$fi["belongsTo"]['fields'][] = $fifi;
					
					
					
				}	
		}
			
		}
		
		$object["fields"][] = $fi;
		
		
	}
	foreach($object["fields"] as $field) { 
		if(array_key_exists("belongsTo",$field)){
			$model = $field["belongsTo"]["model"];
			$field_name = $field["belongsTo"]["foreignKey"];
			$displayField = $field["belongsTo"]["displayField"];
		}
	}	

	$plural = $object["plural"];
	//$Uplural = ucfirst($object["plural"]);
	$singular = $object["singular"];;
	//$Usingular =ucfirst($object["singular"]);
	//var_dump($field_name);
	//var_dump($object);
	?>
<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function url<?php echo $plural ?>(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Track."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.<?php echo $singular; ?>Model', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			<?php foreach($object["fields"] as $field) {
			switch($field["type"]){		
			case "integer":
			?> 
			{       
				 type: 'int',
				 useNull: true,
				name: '<?php echo $field["name"]; ?>'
			},
			<?php
				break;
			default:?> 
			{                   
				name: '<?php echo $field["name"]; ?>',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			<?php
				break;
			}  //end switch
			?>   
			<?php } //end foreach ?>
			
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
		<?php foreach ($object["fields"] as $k=>$v){ 
		if(array_key_exists("belongsTo",$v)){ ?>
		
		
        Ext.define('MyApp.model.<?php echo $v["belongsTo"]["model"] ?>Model', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			<?php foreach($v["belongsTo"]["fields"] as $field) {
			switch($field["type"]){
			case "integer":
			?> 
			{       
				 type: 'int',
				name: '<?php echo $field["name"]; ?>'
			},
			<?php
				break;
			default:?> 
			{                   
				name: '<?php echo $field["name"]; ?>'
			},
			<?php
				break;
				}//end switch
			} ?>//end foreach 			
			]
        });
        Ext.define('MyApp.store.<?php echo $v["belongsTo"]["model"] ?>NomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.<?php echo $v["belongsTo"]["model"] ?>Model',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: '<?php echo $v["belongsTo"]["model"] ?>NomStore', // nume tabela nomenclator
                    model: 'MyApp.model.<?php echo $v["belongsTo"]["model"] ?>Model', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../<?php echo $v["belongsTo"]["model"] ?>s/extdataall',
                        type: 'jsonp',
                        idParam: '<?php echo $v["belongsTo"]["foreignKey"] ?>', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../<?php echo $v["belongsTo"]["model"] ?>s/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["<?php echo $v["belongsTo"]["model"] ?>NomStore"] = Ext.create('MyApp.store.<?php echo $v["belongsTo"]["model"] ?>NomStore');
		<?php } //end if 
		} ?> //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.<?php echo $singular; ?>Store', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.<?php echo $singular; ?>Model',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: '<?php echo $singular; ?>Store', // nume tabela
                    model: 'MyApp.model.<?php echo $singular; ?>Model', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: '<?php echo $object["primaryKey"]; ?>', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return url<?php echo $plural; ?>(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].<?php echo $object["primaryKey"]; ?>;
                } else {
                    id = request.jsonData.<?php echo $object["primaryKey"]; ?>;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.<?php echo $singular; ?>Store');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.<?php echo $plural; ?>Table', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					<?php 
						foreach ($object["fields"] as $k=>$v){ 
						//if ($object["primaryKey"] == $v["name"]) continue;
						//else 
						if(array_key_exists("belongsTo",$v)){
					?> 
						{
							xtype: 'combobox',
							id: 'filter_<?php echo $v["belongsTo"]["foreignKey"]; ?>',
							fieldLabel: '<?php echo $v["belongsTo"]["model"]; ?>',
							store: nomStores["<?php echo $v["belongsTo"]["model"]; ?>NomStore"],
							valueField: '<?php echo $v["belongsTo"]["foreignKey"]; ?>',
							displayField: '<?php echo $v["belongsTo"]["displayField"]; ?>',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							<?php }else{ ?> //end if	
						{
							xtype: 'textfield',
							id: 'filter_<?php echo $v["name"]; ?>',
							fieldLabel: '<?php echo $v["name"]; ?>'
						}, 
							<?php } ?> //end else
						<?php } ?> //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								<?php foreach ($object["fields"] as $k=>$v){  ?>
                                var _val = Ext.getCmp('filter_<?php echo $v["name"]; ?>').getValue();
                                if (_val) {
                                    filter.<?php echo $v["name"]; ?> = _val;
									<?php if ($object["primaryKey"] == $v["name"]) {?>
										filter_op.<?php echo $v["name"]; ?> = "=";
										<?php } else
											if(array_key_exists("belongsTo",$v)){ ?>
											filter_op.<?php echo $v["name"]; ?> = "=";
											
											<?php } else {?>
											filter_op.<?php echo $v["name"]; ?> = "LIKE";
											<?php  } ?>
                                }
								
								
								<?php } ?>
								mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
								<?php foreach ($object["fields"] as $k=>$v){ 
								//if ($object["primaryKey"] == $v["name"]) continue; 
								?>
								
                                Ext.getCmp('filter_<?php echo $v["name"]; ?>').setValue('');
                                
                                
								
								<?php } ?>
								filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
				<?php foreach ($object["fields"] as $k=>$v){ ?>
				<?php if ($object["primaryKey"] == $v["name"]){?> 
				{
                    id: '<?php echo $v["name"]; ?>', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: '<?php echo $v["name"]; ?>', //nume coloana
                    text: '<?php echo $v["name"]; ?>' //nume coloana
                }, 
				<?php }else if(array_key_exists("belongsTo",$v)){?>
				
				{
                    header: '<?php echo $v["belongsTo"]["model"]; ?>', //nume coloana
                    dataIndex: '<?php echo $v["belongsTo"]["foreignKey"]; ?>', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: '<?php echo $v["belongsTo"]["foreignKey"]; ?>',
                        displayField: '<?php echo $v["belongsTo"]["displayField"]; ?>',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["<?php echo $v["belongsTo"]["model"]; ?>NomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["<?php echo $v["belongsTo"]["model"]; ?>NomStore"].findExact('<?php echo $v["belongsTo"]["foreignKey"]; ?>', parseInt(val));
						if(index==-1)
							index = nomStores["<?php echo $v["belongsTo"]["model"]; ?>NomStore"].findExact('<?php echo $v["belongsTo"]["foreignKey"]; ?>', ""+val);
                        if (index != -1) {
                            rs = nomStores["<?php echo $v["belongsTo"]["model"]; ?>NomStore"].getAt(index).data;
                            return rs.<?php echo $v["belongsTo"]["displayField"]; ?>;
                        }
                    }
                },
				<?php }else{ ?>
				{
                    header: '<?php echo $v["name"]; ?>', //nume coloana
                    dataIndex: '<?php echo $v["name"]; ?>', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
				<?php } ?>
				<?php } ?>
				//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        //icon: 'path_to_edit_img',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.Edit<?php echo $singular; ?>Window');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        //icon: 'path_to_img',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                    //icon: 'a.gif',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.Edit<?php echo $singular; ?>Window');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.Edit<?php echo $singular; ?>Window', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add <?php echo $singular; ?>',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
				<?php foreach ($object["fields"] as $k=>$v){
				if(array_key_exists("belongsTo",$v)){?>
				
				{
                    id: 'field_edit_<?php echo $singular; ?>_<?php echo $v["belongsTo"]["model"]; ?>',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: '<?php echo $v["belongsTo"]["model"]; ?>',
                    store: nomStores["<?php echo $v["belongsTo"]["model"]; ?>NomStore"],
                    valueField: '<?php echo $v["belongsTo"]["foreignKey"]; ?>',
                    displayField: '<?php echo $v["belongsTo"]["displayField"]; ?>',
					
                },
				<?php } else{ ?>
				
				{
                    id: 'field_edit_<?php echo $singular; ?>_<?php echo $v["name"]; ?>',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: '<?php echo $v["name"]; ?>'
                }, 
				
				<?php } ?>
				<?php } ?>
				//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									<?php foreach ($object["fields"] as $k=>$v){
									if(array_key_exists("belongsTo",$v)){?>
									
									var _<?php echo $v["name"]; ?> = Ext.getCmp('field_edit_<?php echo $singular; ?>_<?php echo $v["belongsTo"]["model"]; ?>').getValue();
                                    win._edit.set('<?php echo $v["belongsTo"]["foreignKey"]; ?>', _<?php echo $v["belongsTo"]["foreignKey"]; ?>);	
									<?php }else{ ?>
                                    var _<?php echo $v["name"]; ?> = Ext.getCmp('field_edit_<?php echo $singular; ?>_<?php echo $v["name"]; ?>').getValue();
                                    win._edit.set('<?php echo $v["name"]; ?>', _<?php echo $v["name"]; ?>);
                                    
									
									<?php } ?>
									<?php } ?>
									
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
									<?php foreach ($object["fields"] as $k=>$v){
									if(array_key_exists("belongsTo",$v)){?>
									var _<?php echo $v["name"]; ?> = Ext.getCmp('field_edit_<?php echo $singular; ?>_<?php echo $v["belongsTo"]["model"]; ?>').getValue();
									if(_<?php echo $v["name"]; ?>==0) _<?php echo $v["name"]; ?>=null;
									<?php }else{ ?>
									
                                    var _<?php echo $v["name"]; ?> = Ext.getCmp('field_edit_<?php echo $singular; ?>_<?php echo $v["name"]; ?>').getValue();
                                    
									<?php } ?>
									<?php } ?>
									
                                    var r = Ext.ModelManager.create({
										
										<?php foreach ($object["fields"] as $k=>$v){ ?>
										<?php if ($object["primaryKey"] == $v["name"]) continue; ?>
										
                                        <?php echo $v["name"]; ?>: _<?php echo $v["name"]; ?>,
                                        
										<?php } ?>
										
                                    }, 'MyApp.model.<?php echo $singular; ?>Model');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					<?php foreach ($object["fields"] as $k=>$v){
					if(array_key_exists("belongsTo",$v)){?>
					Ext.getCmp('field_edit_<?php echo $singular; ?>_<?php echo $v["belongsTo"]["model"]; ?>').setValue(this._edit.getData().<?php echo $v["belongsTo"]["foreignKey"]; ?>);
					<?php }else{ ?>
                    Ext.getCmp('field_edit_<?php echo $singular; ?>_<?php echo $v["name"]; ?>').setValue(this._edit.getData().<?php echo $v["name"]; ?>);
                    <?php } ?>
                    <?php } ?>
					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
				<?php foreach ($object["fields"] as $k=>$v){
				if(array_key_exists("belongsTo",$v)){?>
				'<?php echo $v["belongsTo"]["model"]; ?>Model',
				<?php } ?>
				<?php } ?>		
                '<?php echo $singular; ?>Model',
                
				
				
            ],
            stores: [
				<?php foreach ($object["fields"] as $k=>$v){
				if(array_key_exists("belongsTo",$v)){?>
				'<?php echo $v["belongsTo"]["model"]; ?>NomStore',
				<?php } ?>
				<?php } ?>	
                '<?php echo $singular; ?>Store',
                
            ],
            views: [
                '<?php echo $plural; ?>Table',
                'Edit<?php echo $singular; ?>Window'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.<?php echo $plural; ?>Table', {
                    renderTo: "<?php echo $plural; ?>Div"
                });
            }

        });
    }
</script>

<div id="<?php echo $plural; ?>Div">
</div>
<script>
    init();
</script>
