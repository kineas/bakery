<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>	
		Portal: <?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		//echo $this->fetch('css');
		echo $this->fetch('script');
		echo $this->Html->script('ext-all-debug.js'); 
		echo $this->Html->script('ext-extensions.js');
		echo $this->Html->script('packages/ext-theme-neptune/build/ext-theme-neptune.js');
		echo $this->Html->css('packages/ext-theme-neptune/build/resources/ext-theme-neptune-all.css');
	?>
	
</head>
<body>
	<div id="container">
		<div id="header">
			<script>
				function initMenu() {
				  Ext.define('AppMenu.view.Menu', {
					extend: 'Ext.panel.Panel',

					requires: [
						'Ext.toolbar.Toolbar',
						'Ext.button.Button',
						'Ext.menu.Menu',
						'Ext.menu.Item'
					],

					
				  

					initComponent: function() {
						var me = this;

						Ext.applyIf(me, {
							dockedItems: [
								{
									xtype: 'toolbar',
									dock: 'top',
									items: [
										{
											xtype: 'button',
											text: 'Music',
											menu: {
												xtype: 'menu',
												width: 120,
												items: [
													{
														xtype: 'menuitem',
														text: 'Assets',
														menu: {
															xtype: 'menu',
															width: 120,
															items: [
																{
																	xtype: 'menuitem',
																	text: 'Albums',
																	listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
																},
																{
																	xtype: 'menuitem',
																	text: 'Tracks',
																	listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
																},
																{
																	xtype: 'menuitem',
																	text: 'Artists',
																	listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
																}
															]
														}
													},
													{
														xtype: 'menuitem',
														text: 'Genres',
														listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
													}
												]
											}
										},
										{
											xtype: 'button',
											text: 'Admin',
											menu: {
												xtype: 'menu',
												width: 120,
												items: [
													
													{
														xtype: 'menuitem',
														text: 'Users',
														menu: {
															xtype: 'menu',
															width: 120,
															items: [
																{
																	xtype: 'menuitem',
																	text: 'Customers',
																	listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
																},
																{
																	xtype: 'menuitem',
																	text: 'Employees',
																	listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
																}
															]
														}
													},
													{
														xtype: 'menuitem',
														text: 'Playlist',
														menu: {
															xtype: 'menu',
															width: 120,
															items: [
																{
																	xtype: 'menuitem',
																	text: 'Playlists',
																	listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
																},
																{
																	xtype: 'menuitem',
																	text: 'PlaylistTracks',
																	listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
																}
															]
														}
													},
													{
														xtype: 'menuitem',
														text: 'Invoices',
														listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
													},
													{
														xtype: 'menuitem',
														text: 'InvoiceLines',
														listeners: {
																		click: {
																			fn: me.onMenuitemClick,
																			scope: me
																		}
																	}
													}
												]
											}
										},
										{
											xtype: 'tbfill'
										},
										{
											xtype: 'button',
											text: 'Log out',
											listeners: {
												click: {
													fn: me.onButtonClick,
													scope: me
												}
											}
										}
									]
								}
							]
						});

						me.callParent(arguments);
					},

					onMenuitemClick: function(item, e, eOpts) {
						window.location.href = '../'+item.text+"/index";
					},

					onButtonClick: function(button, e, eOpts) {
						
					}

				});
				Ext.application({
					id: 'asdfasdf',
					views: [
						'Menu'
					],
					name: 'AppMenu',

					launch: function() {
						Ext.create('AppMenu.view.Menu', {renderTo: "_menuDiv"});
					}

				});
				}
				</script>

				<div id="_menuDiv" >
				</div>
				<script>
					initMenu();
				</script>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
