<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlemployees(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Employee."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.employeeModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'EmployeeId'
			},
			   
			 
			{                   
				name: 'LastName',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'FirstName',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Title',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'ReportsTo'
			},
			   
			 
			{                   
				name: 'BirthDate',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'HireDate',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Address',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'City',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'State',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Country',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'PostalCode',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Phone',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Fax',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Email',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.employeeStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.employeeModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'employeeStore', // nume tabela
                    model: 'MyApp.model.employeeModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'EmployeeId', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlemployees(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].EmployeeId;
                } else {
                    id = request.jsonData.EmployeeId;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.employeeStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.employeesTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_EmployeeId',
							fieldLabel: 'EmployeeId'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_LastName',
							fieldLabel: 'LastName'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_FirstName',
							fieldLabel: 'FirstName'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Title',
							fieldLabel: 'Title'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_ReportsTo',
							fieldLabel: 'ReportsTo'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_BirthDate',
							fieldLabel: 'BirthDate'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_HireDate',
							fieldLabel: 'HireDate'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Address',
							fieldLabel: 'Address'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_City',
							fieldLabel: 'City'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_State',
							fieldLabel: 'State'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Country',
							fieldLabel: 'Country'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_PostalCode',
							fieldLabel: 'PostalCode'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Phone',
							fieldLabel: 'Phone'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Fax',
							fieldLabel: 'Fax'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Email',
							fieldLabel: 'Email'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_EmployeeId').getValue();
                                if (_val) {
                                    filter.EmployeeId = _val;
																			filter_op.EmployeeId = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_LastName').getValue();
                                if (_val) {
                                    filter.LastName = _val;
																				filter_op.LastName = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_FirstName').getValue();
                                if (_val) {
                                    filter.FirstName = _val;
																				filter_op.FirstName = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Title').getValue();
                                if (_val) {
                                    filter.Title = _val;
																				filter_op.Title = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_ReportsTo').getValue();
                                if (_val) {
                                    filter.ReportsTo = _val;
																				filter_op.ReportsTo = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_BirthDate').getValue();
                                if (_val) {
                                    filter.BirthDate = _val;
																				filter_op.BirthDate = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_HireDate').getValue();
                                if (_val) {
                                    filter.HireDate = _val;
																				filter_op.HireDate = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Address').getValue();
                                if (_val) {
                                    filter.Address = _val;
																				filter_op.Address = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_City').getValue();
                                if (_val) {
                                    filter.City = _val;
																				filter_op.City = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_State').getValue();
                                if (_val) {
                                    filter.State = _val;
																				filter_op.State = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Country').getValue();
                                if (_val) {
                                    filter.Country = _val;
																				filter_op.Country = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_PostalCode').getValue();
                                if (_val) {
                                    filter.PostalCode = _val;
																				filter_op.PostalCode = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Phone').getValue();
                                if (_val) {
                                    filter.Phone = _val;
																				filter_op.Phone = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Fax').getValue();
                                if (_val) {
                                    filter.Fax = _val;
																				filter_op.Fax = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Email').getValue();
                                if (_val) {
                                    filter.Email = _val;
																				filter_op.Email = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_EmployeeId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_LastName').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_FirstName').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Title').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_ReportsTo').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_BirthDate').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_HireDate').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Address').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_City').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_State').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Country').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_PostalCode').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Phone').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Fax').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Email').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'EmployeeId', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'EmployeeId', //nume coloana
                    text: 'EmployeeId' //nume coloana
                }, 
																{
                    header: 'LastName', //nume coloana
                    dataIndex: 'LastName', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'FirstName', //nume coloana
                    dataIndex: 'FirstName', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Title', //nume coloana
                    dataIndex: 'Title', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'ReportsTo', //nume coloana
                    dataIndex: 'ReportsTo', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'BirthDate', //nume coloana
                    dataIndex: 'BirthDate', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'HireDate', //nume coloana
                    dataIndex: 'HireDate', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Address', //nume coloana
                    dataIndex: 'Address', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'City', //nume coloana
                    dataIndex: 'City', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'State', //nume coloana
                    dataIndex: 'State', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Country', //nume coloana
                    dataIndex: 'Country', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'PostalCode', //nume coloana
                    dataIndex: 'PostalCode', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Phone', //nume coloana
                    dataIndex: 'Phone', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Fax', //nume coloana
                    dataIndex: 'Fax', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Email', //nume coloana
                    dataIndex: 'Email', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditemployeeWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditemployeeWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditemployeeWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add employee',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_employee_EmployeeId',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'EmployeeId'
                }, 
				
												
				{
                    id: 'field_edit_employee_LastName',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'LastName'
                }, 
				
												
				{
                    id: 'field_edit_employee_FirstName',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'FirstName'
                }, 
				
												
				{
                    id: 'field_edit_employee_Title',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Title'
                }, 
				
												
				{
                    id: 'field_edit_employee_ReportsTo',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'ReportsTo'
                }, 
				
												
				{
                    id: 'field_edit_employee_BirthDate',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'BirthDate'
                }, 
				
												
				{
                    id: 'field_edit_employee_HireDate',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'HireDate'
                }, 
				
												
				{
                    id: 'field_edit_employee_Address',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Address'
                }, 
				
												
				{
                    id: 'field_edit_employee_City',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'City'
                }, 
				
												
				{
                    id: 'field_edit_employee_State',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'State'
                }, 
				
												
				{
                    id: 'field_edit_employee_Country',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Country'
                }, 
				
												
				{
                    id: 'field_edit_employee_PostalCode',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'PostalCode'
                }, 
				
												
				{
                    id: 'field_edit_employee_Phone',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Phone'
                }, 
				
												
				{
                    id: 'field_edit_employee_Fax',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Fax'
                }, 
				
												
				{
                    id: 'field_edit_employee_Email',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Email'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _EmployeeId = Ext.getCmp('field_edit_employee_EmployeeId').getValue();
                                    win._edit.set('EmployeeId', _EmployeeId);
                                    
									
																		                                    var _LastName = Ext.getCmp('field_edit_employee_LastName').getValue();
                                    win._edit.set('LastName', _LastName);
                                    
									
																		                                    var _FirstName = Ext.getCmp('field_edit_employee_FirstName').getValue();
                                    win._edit.set('FirstName', _FirstName);
                                    
									
																		                                    var _Title = Ext.getCmp('field_edit_employee_Title').getValue();
                                    win._edit.set('Title', _Title);
                                    
									
																		                                    var _ReportsTo = Ext.getCmp('field_edit_employee_ReportsTo').getValue();
                                    win._edit.set('ReportsTo', _ReportsTo);
                                    
									
																		                                    var _BirthDate = Ext.getCmp('field_edit_employee_BirthDate').getValue();
                                    win._edit.set('BirthDate', _BirthDate);
                                    
									
																		                                    var _HireDate = Ext.getCmp('field_edit_employee_HireDate').getValue();
                                    win._edit.set('HireDate', _HireDate);
                                    
									
																		                                    var _Address = Ext.getCmp('field_edit_employee_Address').getValue();
                                    win._edit.set('Address', _Address);
                                    
									
																		                                    var _City = Ext.getCmp('field_edit_employee_City').getValue();
                                    win._edit.set('City', _City);
                                    
									
																		                                    var _State = Ext.getCmp('field_edit_employee_State').getValue();
                                    win._edit.set('State', _State);
                                    
									
																		                                    var _Country = Ext.getCmp('field_edit_employee_Country').getValue();
                                    win._edit.set('Country', _Country);
                                    
									
																		                                    var _PostalCode = Ext.getCmp('field_edit_employee_PostalCode').getValue();
                                    win._edit.set('PostalCode', _PostalCode);
                                    
									
																		                                    var _Phone = Ext.getCmp('field_edit_employee_Phone').getValue();
                                    win._edit.set('Phone', _Phone);
                                    
									
																		                                    var _Fax = Ext.getCmp('field_edit_employee_Fax').getValue();
                                    win._edit.set('Fax', _Fax);
                                    
									
																		                                    var _Email = Ext.getCmp('field_edit_employee_Email').getValue();
                                    win._edit.set('Email', _Email);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _EmployeeId = Ext.getCmp('field_edit_employee_EmployeeId').getValue();
                                    
																											
                                    var _LastName = Ext.getCmp('field_edit_employee_LastName').getValue();
                                    
																											
                                    var _FirstName = Ext.getCmp('field_edit_employee_FirstName').getValue();
                                    
																											
                                    var _Title = Ext.getCmp('field_edit_employee_Title').getValue();
                                    
																											
                                    var _ReportsTo = Ext.getCmp('field_edit_employee_ReportsTo').getValue();
                                    
																											
                                    var _BirthDate = Ext.getCmp('field_edit_employee_BirthDate').getValue();
                                    
																											
                                    var _HireDate = Ext.getCmp('field_edit_employee_HireDate').getValue();
                                    
																											
                                    var _Address = Ext.getCmp('field_edit_employee_Address').getValue();
                                    
																											
                                    var _City = Ext.getCmp('field_edit_employee_City').getValue();
                                    
																											
                                    var _State = Ext.getCmp('field_edit_employee_State').getValue();
                                    
																											
                                    var _Country = Ext.getCmp('field_edit_employee_Country').getValue();
                                    
																											
                                    var _PostalCode = Ext.getCmp('field_edit_employee_PostalCode').getValue();
                                    
																											
                                    var _Phone = Ext.getCmp('field_edit_employee_Phone').getValue();
                                    
																											
                                    var _Fax = Ext.getCmp('field_edit_employee_Fax').getValue();
                                    
																											
                                    var _Email = Ext.getCmp('field_edit_employee_Email').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        LastName: _LastName,
                                        
																														
                                        FirstName: _FirstName,
                                        
																														
                                        Title: _Title,
                                        
																														
                                        ReportsTo: _ReportsTo,
                                        
																														
                                        BirthDate: _BirthDate,
                                        
																														
                                        HireDate: _HireDate,
                                        
																														
                                        Address: _Address,
                                        
																														
                                        City: _City,
                                        
																														
                                        State: _State,
                                        
																														
                                        Country: _Country,
                                        
																														
                                        PostalCode: _PostalCode,
                                        
																														
                                        Phone: _Phone,
                                        
																														
                                        Fax: _Fax,
                                        
																														
                                        Email: _Email,
                                        
																				
                                    }, 'MyApp.model.employeeModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_employee_EmployeeId').setValue(this._edit.getData().EmployeeId);
                                                            Ext.getCmp('field_edit_employee_LastName').setValue(this._edit.getData().LastName);
                                                            Ext.getCmp('field_edit_employee_FirstName').setValue(this._edit.getData().FirstName);
                                                            Ext.getCmp('field_edit_employee_Title').setValue(this._edit.getData().Title);
                                                            Ext.getCmp('field_edit_employee_ReportsTo').setValue(this._edit.getData().ReportsTo);
                                                            Ext.getCmp('field_edit_employee_BirthDate').setValue(this._edit.getData().BirthDate);
                                                            Ext.getCmp('field_edit_employee_HireDate').setValue(this._edit.getData().HireDate);
                                                            Ext.getCmp('field_edit_employee_Address').setValue(this._edit.getData().Address);
                                                            Ext.getCmp('field_edit_employee_City').setValue(this._edit.getData().City);
                                                            Ext.getCmp('field_edit_employee_State').setValue(this._edit.getData().State);
                                                            Ext.getCmp('field_edit_employee_Country').setValue(this._edit.getData().Country);
                                                            Ext.getCmp('field_edit_employee_PostalCode').setValue(this._edit.getData().PostalCode);
                                                            Ext.getCmp('field_edit_employee_Phone').setValue(this._edit.getData().Phone);
                                                            Ext.getCmp('field_edit_employee_Fax').setValue(this._edit.getData().Fax);
                                                            Ext.getCmp('field_edit_employee_Email').setValue(this._edit.getData().Email);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
																																																																		
                'employeeModel',
                
				
				
            ],
            stores: [
																																																																	
                'employeeStore',
                
            ],
            views: [
                'employeesTable',
                'EditemployeeWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.employeesTable', {
                    renderTo: "employeesDiv"
                });
            }

        });
    }
</script>

<div id="employeesDiv">
</div>
<script>
    init();
</script>
