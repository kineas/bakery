<?php
/**
* @link          http://cakephp.org CakePHP(tm) Project
* @package       app.View.Layouts
* @since         CakePHP(tm) v 0.10.0.1076
*/

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>	
            Portal: <?php echo $this->fetch('title'); ?>
        </title>
        <?php
        echo $this->Html->meta('icon');

        //echo $this->Html->css('cake.generic');

        echo $this->fetch('meta');
        //echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->Html->script('ext-all-debug.js'); 
        echo $this->Html->script('ext-extensions.js');
        echo $this->Html->script('packages/ext-theme-neptune/build/ext-theme-neptune.js');
        echo $this->Html->css('packages/ext-theme-neptune/build/resources/ext-theme-neptune-all.css');
        ?>


    </head>
    <body>
        <div id="container">
            <div id="header">
                <script>
				function _clickMenu(a,b,c){
					window.location.href = '..'+a.url_navigate;
				}
				
				function logOut(a,b,c){
					window.location.href = '../users/logout/';
				}
				
                        function initMenu() {
                        Ext.define('AppMenu.model.Menu_Model', {
    extend: 'Ext.data.Model',
    requires: [
        // 'Ext.data.field.Boolean',
        // 'Ext.data.field.Date',
        // 'Ext.data.field.Number',
        // 'Ext.data.field.String'
    ],
    fields: [ {       
                         type: 'int',
                         useNull: true,
                        name: 'id'
                },
			   
			 
                {       
                         type: 'int',
                         useNull: true,
                        name: 'acos_id'
                },
			   
			 
                {       
                         type: 'int',
                         useNull: true,
                        name: 'parent_id'
                },
			   
			 
                {                   
                        name: 'name',
                        allowNull: true,
				
                },
			   
			 
                {                   
                        name: 'url',
                        allowNull: true,
				
                },
			   
						
                ]
		});
		
         var def_store = Ext.define('AppMenu.store.Menu_Store', {
				extend: 'Ext.data.Store',
				requires: [
					'AppMenu.model.Menu_Model',
					'Ext.data.proxy.JsonP',
					'Ext.data.reader.Json',
					'Ext.data.writer.Json'
				],
				constructor: function(cfg) {
					var me = this;
					cfg = cfg || {};
					me.callParent([Ext.apply({
						storeId: 'Menu_Store', // nume tabela nomenclator
						model: 'AppMenu.model.Menu_Model', // nume tabela nomenclator
						autoLoad: true,
						proxy: {
							url: '../YMenus/extdataall',
							type: 'jsonp',
							idParam: 'id', // nume id nomenclator                         
							recordParam: '_records',
							callbackKey: '_callback',
							crud: {
								index: '../YMenus/extdataall'
							},
							reader: {
								type: 'json',
								messageProperty: '_message',
								rootProperty: '_root',
								root: "_data",
								successProperty: '_success',
								totalProperty: '_total',
								typeProperty: '_type'
							}
						},
											listeners: {
													load: {
															fn: me.onJsonpstoreLoad,
															scope: me
													}
											}
					}, cfg)]);
				},
					
               
                 onJsonpstoreLoad: function(store, records, successful, eOpts) {
						var records_structured = [];
						var records_array = [];
                        for(var i =0; i<records.length;i++){
							var record = records[i];
							if(record.data.parent_id>0){
								if(record.data.url && record.data.url.length>0)
								{
									records_array[i] = {xtype: 'menuitem',text: record.data.name ,url_navigate: record.data.url,listeners: {click: {fn: _clickMenu}}};
									
								}
								else
								{
									records_array[i] = {xtype: 'menuitem',text: record.data.name  , menu: {xtype: 'menu',width: 120,items: []}};
								}
								
							}
							else{
								records_array[i] = {xtype: 'button',text: record.data.name ,menu: {xtype: 'menu',width: 120,items: []}};
							}	
						}
						for(var i =0; i<records_array.length;i++){
							var record_menu = records_array[i];
							var record = records[i];
							
							if(record_menu.xtype=='button'&&record.data.parent_id==null&&(record.data.url==null||record.data.url=='')){
								records_structured.push(record_menu);
							}
							else{
								for(var j=0;j<records.length;j++){
									//if(records_array[record.data.parent_id]){
									if(records[j].data.id == record.data.parent_id){
										records_array[j].menu.items.push(record_menu);
									}
								}
								
							}
							
						}
						records_structured.push({xtype: 'tbfill'});
						records_structured.push({xtype: 'button',text: 'Log out',listeners: {click: {fn: logOut}}});
						Ext.getCmp('toolbar_menu').add(records_structured);
						console.log(records_structured);
                }
			});

       

        
				
				  Ext.define('AppMenu.view.Menu', {
						extend: 'Ext.panel.Panel',

						requires: [
								'Ext.toolbar.Toolbar',
								'Ext.button.Button',
								'Ext.menu.Menu',
								'Ext.menu.Item'
						],

			
		  

						initComponent: function() {
								var me = this;

								Ext.applyIf(me, {
										dockedItems: [
												{
														xtype: 'toolbar',
														dock: 'top',
														id: 'toolbar_menu',
												}
										]
								});

								me.callParent(arguments);
						},

						onMenuitemClick: function(item, e, eOpts) {
								window.location.href = '../'+item.text+"/index";
						},

						onButtonClick: function(button, e, eOpts) {
				
						}

				});
				Ext.application({
						id: 'asdfasdf',
						models: ['Menu_Model'],
						stores: ['Menu_Store'],
						views: ['Menu'],
						name: 'AppMenu',

						 launch: function() {								 
								 _menu_panel = Ext.create('AppMenu.view.Menu', {renderTo: "_menuDiv"});
						 }

					});
				}
                </script>

                <div id="_menuDiv" >
                </div>
                <script>
                        initMenu();
                </script>
            </div>
            <div id="content">

                <?php echo $this->Session->flash(); ?>

                <?php echo $this->fetch('content'); ?>
            </div>
            <div id="footer">

            </div>
        </div>
        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>
