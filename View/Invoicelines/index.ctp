<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlinvoicelines(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Invoiceline."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.invoicelineModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'InvoiceLineId'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'InvoiceId'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'TrackId'
			},
			   
			 
			{                   
				name: 'UnitPrice',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'Quantity'
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.InvoiceModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'InvoiceId'
			},
			 
			{                   
				name: 'CustomerId'
			},
			 
			{                   
				name: 'InvoiceDate'
			},
			 
			{                   
				name: 'BillingAddress'
			},
			 
			{                   
				name: 'BillingCity'
			},
			 
			{                   
				name: 'BillingState'
			},
			 
			{                   
				name: 'BillingCountry'
			},
			 
			{                   
				name: 'BillingPostalCode'
			},
			 
			{                   
				name: 'Total'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.InvoiceNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.InvoiceModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'InvoiceNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.InvoiceModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Invoices/extdataall',
                        type: 'jsonp',
                        idParam: 'InvoiceId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Invoices/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["InvoiceNomStore"] = Ext.create('MyApp.store.InvoiceNomStore');
				
		
        Ext.define('MyApp.model.TrackModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'TrackId'
			},
			 
			{                   
				name: 'Name'
			},
			 
			{                   
				name: 'AlbumId'
			},
			 
			{                   
				name: 'MediaTypeId'
			},
			 
			{                   
				name: 'GenreId'
			},
			 
			{                   
				name: 'Composer'
			},
			 
			{                   
				name: 'Milliseconds'
			},
			 
			{                   
				name: 'Bytes'
			},
			 
			{                   
				name: 'UnitPrice'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.TrackNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.TrackModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'TrackNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.TrackModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Tracks/extdataall',
                        type: 'jsonp',
                        idParam: 'TrackId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Tracks/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["TrackNomStore"] = Ext.create('MyApp.store.TrackNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.invoicelineStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.invoicelineModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'invoicelineStore', // nume tabela
                    model: 'MyApp.model.invoicelineModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'InvoiceLineId', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlinvoicelines(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].InvoiceLineId;
                } else {
                    id = request.jsonData.InvoiceLineId;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.invoicelineStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.invoicelinesTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_InvoiceLineId',
							fieldLabel: 'InvoiceLineId'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_InvoiceId',
							fieldLabel: 'Invoice',
							store: nomStores["InvoiceNomStore"],
							valueField: 'InvoiceId',
							displayField: 'InvoiceDate',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_TrackId',
							fieldLabel: 'Track',
							store: nomStores["TrackNomStore"],
							valueField: 'TrackId',
							displayField: 'Name',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_UnitPrice',
							fieldLabel: 'UnitPrice'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Quantity',
							fieldLabel: 'Quantity'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_InvoiceLineId').getValue();
                                if (_val) {
                                    filter.InvoiceLineId = _val;
																			filter_op.InvoiceLineId = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_InvoiceId').getValue();
                                if (_val) {
                                    filter.InvoiceId = _val;
																				filter_op.InvoiceId = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_TrackId').getValue();
                                if (_val) {
                                    filter.TrackId = _val;
																				filter_op.TrackId = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_UnitPrice').getValue();
                                if (_val) {
                                    filter.UnitPrice = _val;
																				filter_op.UnitPrice = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Quantity').getValue();
                                if (_val) {
                                    filter.Quantity = _val;
																				filter_op.Quantity = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_InvoiceLineId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_InvoiceId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_TrackId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_UnitPrice').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Quantity').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'InvoiceLineId', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'InvoiceLineId', //nume coloana
                    text: 'InvoiceLineId' //nume coloana
                }, 
																
				{
                    header: 'Invoice', //nume coloana
                    dataIndex: 'InvoiceId', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'InvoiceId',
                        displayField: 'InvoiceDate',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["InvoiceNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["InvoiceNomStore"].findExact('InvoiceId', parseInt(val));
						if(index==-1)
							index = nomStores["InvoiceNomStore"].findExact('InvoiceId', ""+val);
                        if (index != -1) {
                            rs = nomStores["InvoiceNomStore"].getAt(index).data;
                            return rs.InvoiceDate;
                        }
                    }
                },
																
				{
                    header: 'Track', //nume coloana
                    dataIndex: 'TrackId', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'TrackId',
                        displayField: 'Name',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["TrackNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["TrackNomStore"].findExact('TrackId', parseInt(val));
						if(index==-1)
							index = nomStores["TrackNomStore"].findExact('TrackId', ""+val);
                        if (index != -1) {
                            rs = nomStores["TrackNomStore"].getAt(index).data;
                            return rs.Name;
                        }
                    }
                },
																{
                    header: 'UnitPrice', //nume coloana
                    dataIndex: 'UnitPrice', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Quantity', //nume coloana
                    dataIndex: 'Quantity', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditinvoicelineWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditinvoicelineWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditinvoicelineWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add invoiceline',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_invoiceline_InvoiceLineId',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'InvoiceLineId'
                }, 
				
												
				{
                    id: 'field_edit_invoiceline_Invoice',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Invoice',
                    store: nomStores["InvoiceNomStore"],
                    valueField: 'InvoiceId',
                    displayField: 'InvoiceDate',
					
                },
												
				{
                    id: 'field_edit_invoiceline_Track',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Track',
                    store: nomStores["TrackNomStore"],
                    valueField: 'TrackId',
                    displayField: 'Name',
					
                },
												
				{
                    id: 'field_edit_invoiceline_UnitPrice',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'UnitPrice'
                }, 
				
												
				{
                    id: 'field_edit_invoiceline_Quantity',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Quantity'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _InvoiceLineId = Ext.getCmp('field_edit_invoiceline_InvoiceLineId').getValue();
                                    win._edit.set('InvoiceLineId', _InvoiceLineId);
                                    
									
																											
									var _InvoiceId = Ext.getCmp('field_edit_invoiceline_Invoice').getValue();
                                    win._edit.set('InvoiceId', _InvoiceId);	
																											
									var _TrackId = Ext.getCmp('field_edit_invoiceline_Track').getValue();
                                    win._edit.set('TrackId', _TrackId);	
																		                                    var _UnitPrice = Ext.getCmp('field_edit_invoiceline_UnitPrice').getValue();
                                    win._edit.set('UnitPrice', _UnitPrice);
                                    
									
																		                                    var _Quantity = Ext.getCmp('field_edit_invoiceline_Quantity').getValue();
                                    win._edit.set('Quantity', _Quantity);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _InvoiceLineId = Ext.getCmp('field_edit_invoiceline_InvoiceLineId').getValue();
                                    
																											var _InvoiceId = Ext.getCmp('field_edit_invoiceline_Invoice').getValue();
									if(_InvoiceId==0) _InvoiceId=null;
																											var _TrackId = Ext.getCmp('field_edit_invoiceline_Track').getValue();
									if(_TrackId==0) _TrackId=null;
																											
                                    var _UnitPrice = Ext.getCmp('field_edit_invoiceline_UnitPrice').getValue();
                                    
																											
                                    var _Quantity = Ext.getCmp('field_edit_invoiceline_Quantity').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        InvoiceId: _InvoiceId,
                                        
																														
                                        TrackId: _TrackId,
                                        
																														
                                        UnitPrice: _UnitPrice,
                                        
																														
                                        Quantity: _Quantity,
                                        
																				
                                    }, 'MyApp.model.invoicelineModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_invoiceline_InvoiceLineId').setValue(this._edit.getData().InvoiceLineId);
                                        					Ext.getCmp('field_edit_invoiceline_Invoice').setValue(this._edit.getData().InvoiceId);
					                    					Ext.getCmp('field_edit_invoiceline_Track').setValue(this._edit.getData().TrackId);
					                                        Ext.getCmp('field_edit_invoiceline_UnitPrice').setValue(this._edit.getData().UnitPrice);
                                                            Ext.getCmp('field_edit_invoiceline_Quantity').setValue(this._edit.getData().Quantity);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
												'InvoiceModel',
												'TrackModel',
																		
                'invoicelineModel',
                
				
				
            ],
            stores: [
												'InvoiceNomStore',
												'TrackNomStore',
																	
                'invoicelineStore',
                
            ],
            views: [
                'invoicelinesTable',
                'EditinvoicelineWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.invoicelinesTable', {
                    renderTo: "invoicelinesDiv"
                });
            }

        });
    }
</script>

<div id="invoicelinesDiv">
</div>
<script>
    init();
</script>
