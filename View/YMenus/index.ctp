<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlyMenus(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":YMenu."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.yMenuModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'id'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'acos_id'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'parent_id'
			},
			   
			 
			{                   
				name: 'name',
				allowNull: true,
				
			},
			   
			 
			{                   
				name: 'url',
				allowNull: true,
				
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.YMenuModel2', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'id'
			},
			 
			{                   
				name: 'acos_id'
			},
			 
			{                   
				name: 'parent_id'
			},
			 
			{                   
				name: 'name'
			},
			 
			{                   
				name: 'url'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.YMenuNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.YMenuModel2',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'YMenuNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.YMenuModel2', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../YMenus/extdataall',
                        type: 'jsonp',
                        idParam: 'id', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../YMenus/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["YMenuNomStore"] = Ext.create('MyApp.store.YMenuNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.yMenuStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.yMenuModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'yMenuStore', // nume tabela
                    model: 'MyApp.model.yMenuModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'id', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlyMenus(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].id;
                } else {
                    id = request.jsonData.id;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.yMenuStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.yMenusTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_id',
							fieldLabel: 'id'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_acos_id',
							fieldLabel: 'acos_id'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_id',
							fieldLabel: 'YMenu',
							store: nomStores["YMenuNomStore"],
							valueField: 'id',
							displayField: 'name',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_name',
							fieldLabel: 'name'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_url',
							fieldLabel: 'url'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_id').getValue();
                                if (_val) {
                                    filter.id = _val;
																			filter_op.id = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_acos_id').getValue();
                                if (_val) {
                                    filter.acos_id = _val;
																				filter_op.acos_id = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_parent_id').getValue();
                                if (_val) {
                                    filter.parent_id = _val;
																				filter_op.parent_id = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_name').getValue();
                                if (_val) {
                                    filter.name = _val;
																				filter_op.name = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_url').getValue();
                                if (_val) {
                                    filter.url = _val;
																				filter_op.url = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_acos_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_parent_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_name').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_url').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'id', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'id', //nume coloana
                    text: 'id' //nume coloana
                }, 
																{
                    header: 'acos_id', //nume coloana
                    dataIndex: 'acos_id', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																
				{
                    header: 'YMenu', //nume coloana
                    dataIndex: 'parent_id', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'id',
                        displayField: 'name',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["YMenuNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["YMenuNomStore"].findExact('id', parseInt(val));
						if(index==-1)
							index = nomStores["YMenuNomStore"].findExact('id', ""+val);
                        if (index != -1) {
                            rs = nomStores["YMenuNomStore"].getAt(index).data;
                            return rs.name;
                        }
                    }
                },
																{
                    header: 'name', //nume coloana
                    dataIndex: 'name', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'url', //nume coloana
                    dataIndex: 'url', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EdityMenuWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EdityMenuWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EdityMenuWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add yMenu',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_yMenu_id',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'id'
                }, 
				
												
				{
                    id: 'field_edit_yMenu_acos_id',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'acos_id'
                }, 
				
												
				{
                    id: 'field_edit_yMenu_YMenu',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'YMenu',
                    store: nomStores["YMenuNomStore"],
                    valueField: 'id',
                    displayField: 'name',
					
                },
												
				{
                    id: 'field_edit_yMenu_name',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'name'
                }, 
				
												
				{
                    id: 'field_edit_yMenu_url',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'url'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _id = Ext.getCmp('field_edit_yMenu_id').getValue();
                                    win._edit.set('id', _id);
                                    
									
																		                                    var _acos_id = Ext.getCmp('field_edit_yMenu_acos_id').getValue();
                                    win._edit.set('acos_id', _acos_id);
                                    
									
																											
									var _parent_id = Ext.getCmp('field_edit_yMenu_YMenu').getValue();
                                    win._edit.set('id', _id);	
																		                                    var _name = Ext.getCmp('field_edit_yMenu_name').getValue();
                                    win._edit.set('name', _name);
                                    
									
																		                                    var _url = Ext.getCmp('field_edit_yMenu_url').getValue();
                                    win._edit.set('url', _url);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _id = Ext.getCmp('field_edit_yMenu_id').getValue();
                                    
																											
                                    var _acos_id = Ext.getCmp('field_edit_yMenu_acos_id').getValue();
                                    
																											var _parent_id = Ext.getCmp('field_edit_yMenu_YMenu').getValue();
									if(_parent_id==0) _parent_id=null;
																											
                                    var _name = Ext.getCmp('field_edit_yMenu_name').getValue();
                                    
																											
                                    var _url = Ext.getCmp('field_edit_yMenu_url').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        acos_id: _acos_id,
                                        
																														
                                        parent_id: _parent_id,
                                        
																														
                                        name: _name,
                                        
																														
                                        url: _url,
                                        
																				
                                    }, 'MyApp.model.yMenuModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_yMenu_id').setValue(this._edit.getData().id);
                                                            Ext.getCmp('field_edit_yMenu_acos_id').setValue(this._edit.getData().acos_id);
                                        					Ext.getCmp('field_edit_yMenu_YMenu').setValue(this._edit.getData().id_parent);
					                                        Ext.getCmp('field_edit_yMenu_name').setValue(this._edit.getData().name);
                                                            Ext.getCmp('field_edit_yMenu_url').setValue(this._edit.getData().url);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
																'YMenuModel2',
																		
                'yMenuModel',
                
				
				
            ],
            stores: [
																'YMenuNomStore',
																	
                'yMenuStore',
                
            ],
            views: [
                'yMenusTable',
                'EdityMenuWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.yMenusTable', {
                    renderTo: "yMenusDiv"
                });
            }

        });
    }
</script>

<div id="yMenusDiv">
</div>
<script>
    init();
</script>
