<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlcustomers(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Customer."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.customerModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'CustomerId'
			},
			   
			 
			{                   
				name: 'FirstName',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'LastName',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Company',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Address',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'City',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'State',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Country',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'PostalCode',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Phone',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Fax',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Email',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'SupportRepId'
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.EmployeeModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'EmployeeId'
			},
			 
			{                   
				name: 'LastName'
			},
			 
			{                   
				name: 'FirstName'
			},
			 
			{                   
				name: 'Title'
			},
			 
			{                   
				name: 'ReportsTo'
			},
			 
			{                   
				name: 'BirthDate'
			},
			 
			{                   
				name: 'HireDate'
			},
			 
			{                   
				name: 'Address'
			},
			 
			{                   
				name: 'City'
			},
			 
			{                   
				name: 'State'
			},
			 
			{                   
				name: 'Country'
			},
			 
			{                   
				name: 'PostalCode'
			},
			 
			{                   
				name: 'Phone'
			},
			 
			{                   
				name: 'Fax'
			},
			 
			{                   
				name: 'Email'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.EmployeeNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.EmployeeModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'EmployeeNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.EmployeeModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Employees/extdataall',
                        type: 'jsonp',
                        idParam: 'EmployeeId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Employees/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["EmployeeNomStore"] = Ext.create('MyApp.store.EmployeeNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.customerStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.customerModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'customerStore', // nume tabela
                    model: 'MyApp.model.customerModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'CustomerId', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlcustomers(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].CustomerId;
                } else {
                    id = request.jsonData.CustomerId;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.customerStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.customersTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_CustomerId',
							fieldLabel: 'CustomerId'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_FirstName',
							fieldLabel: 'FirstName'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_LastName',
							fieldLabel: 'LastName'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Company',
							fieldLabel: 'Company'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Address',
							fieldLabel: 'Address'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_City',
							fieldLabel: 'City'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_State',
							fieldLabel: 'State'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Country',
							fieldLabel: 'Country'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_PostalCode',
							fieldLabel: 'PostalCode'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Phone',
							fieldLabel: 'Phone'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Fax',
							fieldLabel: 'Fax'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Email',
							fieldLabel: 'Email'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_EmployeeId',
							fieldLabel: 'Employee',
							store: nomStores["EmployeeNomStore"],
							valueField: 'EmployeeId',
							displayField: 'LastName',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_CustomerId').getValue();
                                if (_val) {
                                    filter.CustomerId = _val;
																			filter_op.CustomerId = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_FirstName').getValue();
                                if (_val) {
                                    filter.FirstName = _val;
																				filter_op.FirstName = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_LastName').getValue();
                                if (_val) {
                                    filter.LastName = _val;
																				filter_op.LastName = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Company').getValue();
                                if (_val) {
                                    filter.Company = _val;
																				filter_op.Company = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Address').getValue();
                                if (_val) {
                                    filter.Address = _val;
																				filter_op.Address = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_City').getValue();
                                if (_val) {
                                    filter.City = _val;
																				filter_op.City = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_State').getValue();
                                if (_val) {
                                    filter.State = _val;
																				filter_op.State = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Country').getValue();
                                if (_val) {
                                    filter.Country = _val;
																				filter_op.Country = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_PostalCode').getValue();
                                if (_val) {
                                    filter.PostalCode = _val;
																				filter_op.PostalCode = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Phone').getValue();
                                if (_val) {
                                    filter.Phone = _val;
																				filter_op.Phone = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Fax').getValue();
                                if (_val) {
                                    filter.Fax = _val;
																				filter_op.Fax = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Email').getValue();
                                if (_val) {
                                    filter.Email = _val;
																				filter_op.Email = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_SupportRepId').getValue();
                                if (_val) {
                                    filter.SupportRepId = _val;
																				filter_op.SupportRepId = "=";
											
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_CustomerId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_FirstName').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_LastName').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Company').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Address').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_City').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_State').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Country').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_PostalCode').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Phone').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Fax').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Email').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_SupportRepId').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'CustomerId', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'CustomerId', //nume coloana
                    text: 'CustomerId' //nume coloana
                }, 
																{
                    header: 'FirstName', //nume coloana
                    dataIndex: 'FirstName', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'LastName', //nume coloana
                    dataIndex: 'LastName', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Company', //nume coloana
                    dataIndex: 'Company', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Address', //nume coloana
                    dataIndex: 'Address', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'City', //nume coloana
                    dataIndex: 'City', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'State', //nume coloana
                    dataIndex: 'State', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Country', //nume coloana
                    dataIndex: 'Country', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'PostalCode', //nume coloana
                    dataIndex: 'PostalCode', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Phone', //nume coloana
                    dataIndex: 'Phone', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Fax', //nume coloana
                    dataIndex: 'Fax', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Email', //nume coloana
                    dataIndex: 'Email', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																
				{
                    header: 'Employee', //nume coloana
                    dataIndex: 'EmployeeId', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'EmployeeId',
                        displayField: 'LastName',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["EmployeeNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["EmployeeNomStore"].findExact('EmployeeId', parseInt(val));
						if(index==-1)
							index = nomStores["EmployeeNomStore"].findExact('EmployeeId', ""+val);
                        if (index != -1) {
                            rs = nomStores["EmployeeNomStore"].getAt(index).data;
                            return rs.LastName;
                        }
                    }
                },
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditcustomerWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditcustomerWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditcustomerWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add customer',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_customer_CustomerId',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'CustomerId'
                }, 
				
												
				{
                    id: 'field_edit_customer_FirstName',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'FirstName'
                }, 
				
												
				{
                    id: 'field_edit_customer_LastName',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'LastName'
                }, 
				
												
				{
                    id: 'field_edit_customer_Company',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Company'
                }, 
				
												
				{
                    id: 'field_edit_customer_Address',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Address'
                }, 
				
												
				{
                    id: 'field_edit_customer_City',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'City'
                }, 
				
												
				{
                    id: 'field_edit_customer_State',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'State'
                }, 
				
												
				{
                    id: 'field_edit_customer_Country',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Country'
                }, 
				
												
				{
                    id: 'field_edit_customer_PostalCode',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'PostalCode'
                }, 
				
												
				{
                    id: 'field_edit_customer_Phone',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Phone'
                }, 
				
												
				{
                    id: 'field_edit_customer_Fax',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Fax'
                }, 
				
												
				{
                    id: 'field_edit_customer_Email',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Email'
                }, 
				
												
				{
                    id: 'field_edit_customer_Employee',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Employee',
                    store: nomStores["EmployeeNomStore"],
                    valueField: 'EmployeeId',
                    displayField: 'LastName',
					
                },
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _CustomerId = Ext.getCmp('field_edit_customer_CustomerId').getValue();
                                    win._edit.set('CustomerId', _CustomerId);
                                    
									
																		                                    var _FirstName = Ext.getCmp('field_edit_customer_FirstName').getValue();
                                    win._edit.set('FirstName', _FirstName);
                                    
									
																		                                    var _LastName = Ext.getCmp('field_edit_customer_LastName').getValue();
                                    win._edit.set('LastName', _LastName);
                                    
									
																		                                    var _Company = Ext.getCmp('field_edit_customer_Company').getValue();
                                    win._edit.set('Company', _Company);
                                    
									
																		                                    var _Address = Ext.getCmp('field_edit_customer_Address').getValue();
                                    win._edit.set('Address', _Address);
                                    
									
																		                                    var _City = Ext.getCmp('field_edit_customer_City').getValue();
                                    win._edit.set('City', _City);
                                    
									
																		                                    var _State = Ext.getCmp('field_edit_customer_State').getValue();
                                    win._edit.set('State', _State);
                                    
									
																		                                    var _Country = Ext.getCmp('field_edit_customer_Country').getValue();
                                    win._edit.set('Country', _Country);
                                    
									
																		                                    var _PostalCode = Ext.getCmp('field_edit_customer_PostalCode').getValue();
                                    win._edit.set('PostalCode', _PostalCode);
                                    
									
																		                                    var _Phone = Ext.getCmp('field_edit_customer_Phone').getValue();
                                    win._edit.set('Phone', _Phone);
                                    
									
																		                                    var _Fax = Ext.getCmp('field_edit_customer_Fax').getValue();
                                    win._edit.set('Fax', _Fax);
                                    
									
																		                                    var _Email = Ext.getCmp('field_edit_customer_Email').getValue();
                                    win._edit.set('Email', _Email);
                                    
									
																											
									var _SupportRepId = Ext.getCmp('field_edit_customer_Employee').getValue();
                                    win._edit.set('EmployeeId', _EmployeeId);	
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _CustomerId = Ext.getCmp('field_edit_customer_CustomerId').getValue();
                                    
																											
                                    var _FirstName = Ext.getCmp('field_edit_customer_FirstName').getValue();
                                    
																											
                                    var _LastName = Ext.getCmp('field_edit_customer_LastName').getValue();
                                    
																											
                                    var _Company = Ext.getCmp('field_edit_customer_Company').getValue();
                                    
																											
                                    var _Address = Ext.getCmp('field_edit_customer_Address').getValue();
                                    
																											
                                    var _City = Ext.getCmp('field_edit_customer_City').getValue();
                                    
																											
                                    var _State = Ext.getCmp('field_edit_customer_State').getValue();
                                    
																											
                                    var _Country = Ext.getCmp('field_edit_customer_Country').getValue();
                                    
																											
                                    var _PostalCode = Ext.getCmp('field_edit_customer_PostalCode').getValue();
                                    
																											
                                    var _Phone = Ext.getCmp('field_edit_customer_Phone').getValue();
                                    
																											
                                    var _Fax = Ext.getCmp('field_edit_customer_Fax').getValue();
                                    
																											
                                    var _Email = Ext.getCmp('field_edit_customer_Email').getValue();
                                    
																											var _SupportRepId = Ext.getCmp('field_edit_customer_Employee').getValue();
									if(_SupportRepId==0) _SupportRepId=null;
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        FirstName: _FirstName,
                                        
																														
                                        LastName: _LastName,
                                        
																														
                                        Company: _Company,
                                        
																														
                                        Address: _Address,
                                        
																														
                                        City: _City,
                                        
																														
                                        State: _State,
                                        
																														
                                        Country: _Country,
                                        
																														
                                        PostalCode: _PostalCode,
                                        
																														
                                        Phone: _Phone,
                                        
																														
                                        Fax: _Fax,
                                        
																														
                                        Email: _Email,
                                        
																														
                                        SupportRepId: _SupportRepId,
                                        
																				
                                    }, 'MyApp.model.customerModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_customer_CustomerId').setValue(this._edit.getData().CustomerId);
                                                            Ext.getCmp('field_edit_customer_FirstName').setValue(this._edit.getData().FirstName);
                                                            Ext.getCmp('field_edit_customer_LastName').setValue(this._edit.getData().LastName);
                                                            Ext.getCmp('field_edit_customer_Company').setValue(this._edit.getData().Company);
                                                            Ext.getCmp('field_edit_customer_Address').setValue(this._edit.getData().Address);
                                                            Ext.getCmp('field_edit_customer_City').setValue(this._edit.getData().City);
                                                            Ext.getCmp('field_edit_customer_State').setValue(this._edit.getData().State);
                                                            Ext.getCmp('field_edit_customer_Country').setValue(this._edit.getData().Country);
                                                            Ext.getCmp('field_edit_customer_PostalCode').setValue(this._edit.getData().PostalCode);
                                                            Ext.getCmp('field_edit_customer_Phone').setValue(this._edit.getData().Phone);
                                                            Ext.getCmp('field_edit_customer_Fax').setValue(this._edit.getData().Fax);
                                                            Ext.getCmp('field_edit_customer_Email').setValue(this._edit.getData().Email);
                                        					Ext.getCmp('field_edit_customer_Employee').setValue(this._edit.getData().EmployeeId);
					                    					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
																																																								'EmployeeModel',
										
                'customerModel',
                
				
				
            ],
            stores: [
																																																								'EmployeeNomStore',
									
                'customerStore',
                
            ],
            views: [
                'customersTable',
                'EditcustomerWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.customersTable', {
                    renderTo: "customersDiv"
                });
            }

        });
    }
</script>

<div id="customersDiv">
</div>
<script>
    init();
</script>
