<?php echo $this->request->query['_callback']; ?>({
	"_success":true,
	"_total" :"<?php echo $this->Paginator->counter(array('format' => __('{:count}'))); ?>", 
	"_message":"Loaded data",	
	"_data":[
    <?php foreach ($groups as $group): ?>
			{"id":<?php if($group['Group']['id']) {echo $group['Group']['id'].','; } else {  echo 'null,';}?>"name":<?php $x= json_encode($group['Group']['name']); if($x!='') echo $x; else echo '""';?>,"created":<?php $x= json_encode($group['Group']['created']); if($x!='') echo $x; else echo '""';?>,"modified":<?php $x= json_encode($group['Group']['modified']); if($x!='') echo $x; else echo '""';?>},	
		
<?php endforeach; ?>	]
});
