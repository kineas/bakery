<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlgroups(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Group."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.groupModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'id'
			},
			   
			 
			{                   
				name: 'name',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'created',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'modified',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.groupStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.groupModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'groupStore', // nume tabela
                    model: 'MyApp.model.groupModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'id', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlgroups(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].id;
                } else {
                    id = request.jsonData.id;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.groupStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.groupsTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_id',
							fieldLabel: 'id'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_name',
							fieldLabel: 'name'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_created',
							fieldLabel: 'created'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_modified',
							fieldLabel: 'modified'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_id').getValue();
                                if (_val) {
                                    filter.id = _val;
																			filter_op.id = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_name').getValue();
                                if (_val) {
                                    filter.name = _val;
																				filter_op.name = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_created').getValue();
                                if (_val) {
                                    filter.created = _val;
																				filter_op.created = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_modified').getValue();
                                if (_val) {
                                    filter.modified = _val;
																				filter_op.modified = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_name').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_created').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_modified').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'id', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'id', //nume coloana
                    text: 'id' //nume coloana
                }, 
																{
                    header: 'name', //nume coloana
                    dataIndex: 'name', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'created', //nume coloana
                    dataIndex: 'created', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'modified', //nume coloana
                    dataIndex: 'modified', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditgroupWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditgroupWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditgroupWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add group',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_group_id',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'id'
                }, 
				
												
				{
                    id: 'field_edit_group_name',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'name'
                }, 
				
												
				{
                    id: 'field_edit_group_created',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'created'
                }, 
				
												
				{
                    id: 'field_edit_group_modified',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'modified'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _id = Ext.getCmp('field_edit_group_id').getValue();
                                    win._edit.set('id', _id);
                                    
									
																		                                    var _name = Ext.getCmp('field_edit_group_name').getValue();
                                    win._edit.set('name', _name);
                                    
									
																		                                    var _created = Ext.getCmp('field_edit_group_created').getValue();
                                    win._edit.set('created', _created);
                                    
									
																		                                    var _modified = Ext.getCmp('field_edit_group_modified').getValue();
                                    win._edit.set('modified', _modified);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _id = Ext.getCmp('field_edit_group_id').getValue();
                                    
																											
                                    var _name = Ext.getCmp('field_edit_group_name').getValue();
                                    
																											
                                    var _created = Ext.getCmp('field_edit_group_created').getValue();
                                    
																											
                                    var _modified = Ext.getCmp('field_edit_group_modified').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        name: _name,
                                        
																														
                                        created: _created,
                                        
																														
                                        modified: _modified,
                                        
																				
                                    }, 'MyApp.model.groupModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_group_id').setValue(this._edit.getData().id);
                                                            Ext.getCmp('field_edit_group_name').setValue(this._edit.getData().name);
                                                            Ext.getCmp('field_edit_group_created').setValue(this._edit.getData().created);
                                                            Ext.getCmp('field_edit_group_modified').setValue(this._edit.getData().modified);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
																						
                'groupModel',
                
				
				
            ],
            stores: [
																					
                'groupStore',
                
            ],
            views: [
                'groupsTable',
                'EditgroupWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.groupsTable', {
                    renderTo: "groupsDiv"
                });
            }

        });
    }
</script>

<div id="groupsDiv">
</div>
<script>
    init();
</script>
