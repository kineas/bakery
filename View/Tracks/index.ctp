<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urltracks(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Track."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.trackModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'TrackId'
			},
			   
			 
			{                   
				name: 'Name',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'AlbumId'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'MediaTypeId'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'GenreId'
			},
			   
			 
			{                   
				name: 'Composer',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'Milliseconds'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'Bytes'
			},
			   
			 
			{                   
				name: 'UnitPrice',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.AlbumModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'AlbumId'
			},
			 
			{                   
				name: 'Title'
			},
			 
			{                   
				name: 'ArtistId'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.AlbumNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.AlbumModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'AlbumNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.AlbumModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Albums/extdataall',
                        type: 'jsonp',
                        idParam: 'AlbumId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Albums/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["AlbumNomStore"] = Ext.create('MyApp.store.AlbumNomStore');
				
		
        Ext.define('MyApp.model.MediaTypeModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'MediaTypeId'
			},
			 
			{                   
				name: 'Name'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.MediaTypeNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.MediaTypeModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'MediaTypeNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.MediaTypeModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../MediaTypes/extdataall',
                        type: 'jsonp',
                        idParam: 'MediaTypeId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../MediaTypes/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["MediaTypeNomStore"] = Ext.create('MyApp.store.MediaTypeNomStore');
				
		
        Ext.define('MyApp.model.GenreModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'GenreId'
			},
			 
			{                   
				name: 'Name'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.GenreNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.GenreModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'GenreNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.GenreModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Genres/extdataall',
                        type: 'jsonp',
                        idParam: 'GenreId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Genres/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["GenreNomStore"] = Ext.create('MyApp.store.GenreNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.trackStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.trackModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'trackStore', // nume tabela
                    model: 'MyApp.model.trackModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'TrackId', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urltracks(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].TrackId;
                } else {
                    id = request.jsonData.TrackId;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.trackStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.tracksTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_TrackId',
							fieldLabel: 'TrackId'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Name',
							fieldLabel: 'Name'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_AlbumId',
							fieldLabel: 'Album',
							store: nomStores["AlbumNomStore"],
							valueField: 'AlbumId',
							displayField: 'Title',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_MediaTypeId',
							fieldLabel: 'MediaType',
							store: nomStores["MediaTypeNomStore"],
							valueField: 'MediaTypeId',
							displayField: 'Name',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_GenreId',
							fieldLabel: 'Genre',
							store: nomStores["GenreNomStore"],
							valueField: 'GenreId',
							displayField: 'Name',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Composer',
							fieldLabel: 'Composer'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Milliseconds',
							fieldLabel: 'Milliseconds'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Bytes',
							fieldLabel: 'Bytes'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_UnitPrice',
							fieldLabel: 'UnitPrice'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_TrackId').getValue();
                                if (_val) {
                                    filter.TrackId = _val;
																			filter_op.TrackId = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_Name').getValue();
                                if (_val) {
                                    filter.Name = _val;
																				filter_op.Name = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_AlbumId').getValue();
                                if (_val) {
                                    filter.AlbumId = _val;
																				filter_op.AlbumId = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_MediaTypeId').getValue();
                                if (_val) {
                                    filter.MediaTypeId = _val;
																				filter_op.MediaTypeId = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_GenreId').getValue();
                                if (_val) {
                                    filter.GenreId = _val;
																				filter_op.GenreId = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Composer').getValue();
                                if (_val) {
                                    filter.Composer = _val;
																				filter_op.Composer = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Milliseconds').getValue();
                                if (_val) {
                                    filter.Milliseconds = _val;
																				filter_op.Milliseconds = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Bytes').getValue();
                                if (_val) {
                                    filter.Bytes = _val;
																				filter_op.Bytes = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_UnitPrice').getValue();
                                if (_val) {
                                    filter.UnitPrice = _val;
																				filter_op.UnitPrice = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_TrackId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Name').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_AlbumId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_MediaTypeId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_GenreId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Composer').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Milliseconds').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Bytes').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_UnitPrice').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'TrackId', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'TrackId', //nume coloana
                    text: 'TrackId' //nume coloana
                }, 
																{
                    header: 'Name', //nume coloana
                    dataIndex: 'Name', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																
				{
                    header: 'Album', //nume coloana
                    dataIndex: 'AlbumId', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'AlbumId',
                        displayField: 'Title',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["AlbumNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["AlbumNomStore"].findExact('AlbumId', parseInt(val));
						if(index==-1)
							index = nomStores["AlbumNomStore"].findExact('AlbumId', ""+val);
                        if (index != -1) {
                            rs = nomStores["AlbumNomStore"].getAt(index).data;
                            return rs.Title;
                        }
                    }
                },
																
				{
                    header: 'MediaType', //nume coloana
                    dataIndex: 'MediaTypeId', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'MediaTypeId',
                        displayField: 'Name',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["MediaTypeNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["MediaTypeNomStore"].findExact('MediaTypeId', parseInt(val));
						if(index==-1)
							index = nomStores["MediaTypeNomStore"].findExact('MediaTypeId', ""+val);
                        if (index != -1) {
                            rs = nomStores["MediaTypeNomStore"].getAt(index).data;
                            return rs.Name;
                        }
                    }
                },
																
				{
                    header: 'Genre', //nume coloana
                    dataIndex: 'GenreId', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'GenreId',
                        displayField: 'Name',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["GenreNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["GenreNomStore"].findExact('GenreId', parseInt(val));
						if(index==-1)
							index = nomStores["GenreNomStore"].findExact('GenreId', ""+val);
                        if (index != -1) {
                            rs = nomStores["GenreNomStore"].getAt(index).data;
                            return rs.Name;
                        }
                    }
                },
																{
                    header: 'Composer', //nume coloana
                    dataIndex: 'Composer', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Milliseconds', //nume coloana
                    dataIndex: 'Milliseconds', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Bytes', //nume coloana
                    dataIndex: 'Bytes', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'UnitPrice', //nume coloana
                    dataIndex: 'UnitPrice', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EdittrackWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EdittrackWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EdittrackWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add track',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_track_TrackId',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'TrackId'
                }, 
				
												
				{
                    id: 'field_edit_track_Name',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Name'
                }, 
				
												
				{
                    id: 'field_edit_track_Album',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Album',
                    store: nomStores["AlbumNomStore"],
                    valueField: 'AlbumId',
                    displayField: 'Title',
					
                },
												
				{
                    id: 'field_edit_track_MediaType',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'MediaType',
                    store: nomStores["MediaTypeNomStore"],
                    valueField: 'MediaTypeId',
                    displayField: 'Name',
					
                },
												
				{
                    id: 'field_edit_track_Genre',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Genre',
                    store: nomStores["GenreNomStore"],
                    valueField: 'GenreId',
                    displayField: 'Name',
					
                },
												
				{
                    id: 'field_edit_track_Composer',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Composer'
                }, 
				
												
				{
                    id: 'field_edit_track_Milliseconds',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Milliseconds'
                }, 
				
												
				{
                    id: 'field_edit_track_Bytes',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Bytes'
                }, 
				
												
				{
                    id: 'field_edit_track_UnitPrice',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'UnitPrice'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _TrackId = Ext.getCmp('field_edit_track_TrackId').getValue();
                                    win._edit.set('TrackId', _TrackId);
                                    
									
																		                                    var _Name = Ext.getCmp('field_edit_track_Name').getValue();
                                    win._edit.set('Name', _Name);
                                    
									
																											
									var _AlbumId = Ext.getCmp('field_edit_track_Album').getValue();
                                    win._edit.set('AlbumId', _AlbumId);	
																											
									var _MediaTypeId = Ext.getCmp('field_edit_track_MediaType').getValue();
                                    win._edit.set('MediaTypeId', _MediaTypeId);	
																											
									var _GenreId = Ext.getCmp('field_edit_track_Genre').getValue();
                                    win._edit.set('GenreId', _GenreId);	
																		                                    var _Composer = Ext.getCmp('field_edit_track_Composer').getValue();
                                    win._edit.set('Composer', _Composer);
                                    
									
																		                                    var _Milliseconds = Ext.getCmp('field_edit_track_Milliseconds').getValue();
                                    win._edit.set('Milliseconds', _Milliseconds);
                                    
									
																		                                    var _Bytes = Ext.getCmp('field_edit_track_Bytes').getValue();
                                    win._edit.set('Bytes', _Bytes);
                                    
									
																		                                    var _UnitPrice = Ext.getCmp('field_edit_track_UnitPrice').getValue();
                                    win._edit.set('UnitPrice', _UnitPrice);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _TrackId = Ext.getCmp('field_edit_track_TrackId').getValue();
                                    
																											
                                    var _Name = Ext.getCmp('field_edit_track_Name').getValue();
                                    
																											var _AlbumId = Ext.getCmp('field_edit_track_Album').getValue();
									if(_AlbumId==0) _AlbumId=null;
																											var _MediaTypeId = Ext.getCmp('field_edit_track_MediaType').getValue();
									if(_MediaTypeId==0) _MediaTypeId=null;
																											var _GenreId = Ext.getCmp('field_edit_track_Genre').getValue();
									if(_GenreId==0) _GenreId=null;
																											
                                    var _Composer = Ext.getCmp('field_edit_track_Composer').getValue();
                                    
																											
                                    var _Milliseconds = Ext.getCmp('field_edit_track_Milliseconds').getValue();
                                    
																											
                                    var _Bytes = Ext.getCmp('field_edit_track_Bytes').getValue();
                                    
																											
                                    var _UnitPrice = Ext.getCmp('field_edit_track_UnitPrice').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        Name: _Name,
                                        
																														
                                        AlbumId: _AlbumId,
                                        
																														
                                        MediaTypeId: _MediaTypeId,
                                        
																														
                                        GenreId: _GenreId,
                                        
																														
                                        Composer: _Composer,
                                        
																														
                                        Milliseconds: _Milliseconds,
                                        
																														
                                        Bytes: _Bytes,
                                        
																														
                                        UnitPrice: _UnitPrice,
                                        
																				
                                    }, 'MyApp.model.trackModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_track_TrackId').setValue(this._edit.getData().TrackId);
                                                            Ext.getCmp('field_edit_track_Name').setValue(this._edit.getData().Name);
                                        					Ext.getCmp('field_edit_track_Album').setValue(this._edit.getData().AlbumId);
					                    					Ext.getCmp('field_edit_track_MediaType').setValue(this._edit.getData().MediaTypeId);
					                    					Ext.getCmp('field_edit_track_Genre').setValue(this._edit.getData().GenreId);
					                                        Ext.getCmp('field_edit_track_Composer').setValue(this._edit.getData().Composer);
                                                            Ext.getCmp('field_edit_track_Milliseconds').setValue(this._edit.getData().Milliseconds);
                                                            Ext.getCmp('field_edit_track_Bytes').setValue(this._edit.getData().Bytes);
                                                            Ext.getCmp('field_edit_track_UnitPrice').setValue(this._edit.getData().UnitPrice);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
																'AlbumModel',
												'MediaTypeModel',
												'GenreModel',
																										
                'trackModel',
                
				
				
            ],
            stores: [
																'AlbumNomStore',
												'MediaTypeNomStore',
												'GenreNomStore',
																									
                'trackStore',
                
            ],
            views: [
                'tracksTable',
                'EdittrackWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.tracksTable', {
                    renderTo: "tracksDiv"
                });
            }

        });
    }
</script>

<div id="tracksDiv">
</div>
<script>
    init();
</script>
