<?php echo $this->request->query['_callback']; ?>({
	"_success":true,	
	"_message":"Loaded data",	
	"_data":[
    <?php foreach ($tracks as $track): ?>
			{"TrackId":<?php if($track['Track']['TrackId']) {echo $track['Track']['TrackId'].','; } else {  echo 'null,';}?>"Name":<?php $x= json_encode($track['Track']['Name']); if($x!='') echo $x; else echo '""';?>,"AlbumId":<?php if($track['Track']['AlbumId']) {echo $track['Track']['AlbumId'].','; } else {  echo 'null,';}?>"MediaTypeId":<?php if($track['Track']['MediaTypeId']) {echo $track['Track']['MediaTypeId'].','; } else {  echo 'null,';}?>"GenreId":<?php if($track['Track']['GenreId']) {echo $track['Track']['GenreId'].','; } else {  echo 'null,';}?>"Composer":<?php $x= json_encode($track['Track']['Composer']); if($x!='') echo $x; else echo '""';?>,"Milliseconds":<?php if($track['Track']['Milliseconds']) {echo $track['Track']['Milliseconds'].','; } else {  echo 'null,';}?>"Bytes":<?php if($track['Track']['Bytes']) {echo $track['Track']['Bytes'].','; } else {  echo 'null,';}?>"UnitPrice":<?php $x= json_encode($track['Track']['UnitPrice']); if($x!='') echo $x; else echo '""';?>},	
		
<?php endforeach; ?>	]
});
