<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlacos(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Aco."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.acoModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'id'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'parent_id'
			},
			   
			 
			{                   
				name: 'model',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'foreign_key'
			},
			   
			 
			{                   
				name: 'alias',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'lft'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'rght'
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.acoStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.acoModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'acoStore', // nume tabela
                    model: 'MyApp.model.acoModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'id', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlacos(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].id;
                } else {
                    id = request.jsonData.id;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.acoStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.acosTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_id',
							fieldLabel: 'id'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_parent_id',
							fieldLabel: 'parent_id'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_model',
							fieldLabel: 'model'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_foreign_key',
							fieldLabel: 'foreign_key'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_alias',
							fieldLabel: 'alias'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_lft',
							fieldLabel: 'lft'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_rght',
							fieldLabel: 'rght'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_id').getValue();
                                if (_val) {
                                    filter.id = _val;
																			filter_op.id = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_parent_id').getValue();
                                if (_val) {
                                    filter.parent_id = _val;
																				filter_op.parent_id = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_model').getValue();
                                if (_val) {
                                    filter.model = _val;
																				filter_op.model = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_foreign_key').getValue();
                                if (_val) {
                                    filter.foreign_key = _val;
																				filter_op.foreign_key = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_alias').getValue();
                                if (_val) {
                                    filter.alias = _val;
																				filter_op.alias = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_lft').getValue();
                                if (_val) {
                                    filter.lft = _val;
																				filter_op.lft = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_rght').getValue();
                                if (_val) {
                                    filter.rght = _val;
																				filter_op.rght = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_parent_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_model').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_foreign_key').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_alias').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_lft').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_rght').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'id', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'id', //nume coloana
                    text: 'id' //nume coloana
                }, 
																{
                    header: 'parent_id', //nume coloana
                    dataIndex: 'parent_id', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'model', //nume coloana
                    dataIndex: 'model', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'foreign_key', //nume coloana
                    dataIndex: 'foreign_key', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'alias', //nume coloana
                    dataIndex: 'alias', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'lft', //nume coloana
                    dataIndex: 'lft', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'rght', //nume coloana
                    dataIndex: 'rght', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditacoWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditacoWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditacoWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add aco',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_aco_id',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'id'
                }, 
				
												
				{
                    id: 'field_edit_aco_parent_id',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'parent_id'
                }, 
				
												
				{
                    id: 'field_edit_aco_model',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'model'
                }, 
				
												
				{
                    id: 'field_edit_aco_foreign_key',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'foreign_key'
                }, 
				
												
				{
                    id: 'field_edit_aco_alias',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'alias'
                }, 
				
												
				{
                    id: 'field_edit_aco_lft',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'lft'
                }, 
				
												
				{
                    id: 'field_edit_aco_rght',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'rght'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _id = Ext.getCmp('field_edit_aco_id').getValue();
                                    win._edit.set('id', _id);
                                    
									
																		                                    var _parent_id = Ext.getCmp('field_edit_aco_parent_id').getValue();
                                    win._edit.set('parent_id', _parent_id);
                                    
									
																		                                    var _model = Ext.getCmp('field_edit_aco_model').getValue();
                                    win._edit.set('model', _model);
                                    
									
																		                                    var _foreign_key = Ext.getCmp('field_edit_aco_foreign_key').getValue();
                                    win._edit.set('foreign_key', _foreign_key);
                                    
									
																		                                    var _alias = Ext.getCmp('field_edit_aco_alias').getValue();
                                    win._edit.set('alias', _alias);
                                    
									
																		                                    var _lft = Ext.getCmp('field_edit_aco_lft').getValue();
                                    win._edit.set('lft', _lft);
                                    
									
																		                                    var _rght = Ext.getCmp('field_edit_aco_rght').getValue();
                                    win._edit.set('rght', _rght);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _id = Ext.getCmp('field_edit_aco_id').getValue();
                                    
																											
                                    var _parent_id = Ext.getCmp('field_edit_aco_parent_id').getValue();
                                    
																											
                                    var _model = Ext.getCmp('field_edit_aco_model').getValue();
                                    
																											
                                    var _foreign_key = Ext.getCmp('field_edit_aco_foreign_key').getValue();
                                    
																											
                                    var _alias = Ext.getCmp('field_edit_aco_alias').getValue();
                                    
																											
                                    var _lft = Ext.getCmp('field_edit_aco_lft').getValue();
                                    
																											
                                    var _rght = Ext.getCmp('field_edit_aco_rght').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        parent_id: _parent_id,
                                        
																														
                                        model: _model,
                                        
																														
                                        foreign_key: _foreign_key,
                                        
																														
                                        alias: _alias,
                                        
																														
                                        lft: _lft,
                                        
																														
                                        rght: _rght,
                                        
																				
                                    }, 'MyApp.model.acoModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_aco_id').setValue(this._edit.getData().id);
                                                            Ext.getCmp('field_edit_aco_parent_id').setValue(this._edit.getData().parent_id);
                                                            Ext.getCmp('field_edit_aco_model').setValue(this._edit.getData().model);
                                                            Ext.getCmp('field_edit_aco_foreign_key').setValue(this._edit.getData().foreign_key);
                                                            Ext.getCmp('field_edit_aco_alias').setValue(this._edit.getData().alias);
                                                            Ext.getCmp('field_edit_aco_lft').setValue(this._edit.getData().lft);
                                                            Ext.getCmp('field_edit_aco_rght').setValue(this._edit.getData().rght);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
																																		
                'acoModel',
                
				
				
            ],
            stores: [
																																	
                'acoStore',
                
            ],
            views: [
                'acosTable',
                'EditacoWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.acosTable', {
                    renderTo: "acosDiv"
                });
            }

        });
    }
</script>

<div id="acosDiv">
</div>
<script>
    init();
</script>
