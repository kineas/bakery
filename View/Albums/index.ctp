<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlalbums(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Album."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.albumModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'AlbumId'
			},
			   
			 
			{                   
				name: 'Title',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'ArtistId'
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.ArtistModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'ArtistId'
			},
			 
			{                   
				name: 'Name'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.ArtistNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.ArtistModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'ArtistNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.ArtistModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Artists/extdataall',
                        type: 'jsonp',
                        idParam: 'ArtistId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Artists/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["ArtistNomStore"] = Ext.create('MyApp.store.ArtistNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.albumStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.albumModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'albumStore', // nume tabela
                    model: 'MyApp.model.albumModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: '<?php echo $this->webroot; ?>/albums/extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'AlbumId', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlalbums(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].AlbumId;
                } else {
                    id = request.jsonData.AlbumId;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.albumStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.albumsTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_AlbumId',
							fieldLabel: 'AlbumId'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Title',
							fieldLabel: 'Title'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_ArtistId',
							fieldLabel: 'Artist',
							store: nomStores["ArtistNomStore"],
							valueField: 'ArtistId',
							displayField: 'Name',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_AlbumId').getValue();
                                if (_val) {
                                    filter.AlbumId = _val;
																			filter_op.AlbumId = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_Title').getValue();
                                if (_val) {
                                    filter.Title = _val;
																				filter_op.Title = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_ArtistId').getValue();
                                if (_val) {
                                    filter.ArtistId = _val;
																				filter_op.ArtistId = "=";
											
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_AlbumId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Title').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_ArtistId').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'AlbumId', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'AlbumId', //nume coloana
                    text: 'AlbumId' //nume coloana
                }, 
																{
                    header: 'Title', //nume coloana
                    dataIndex: 'Title', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																
				{
                    header: 'Artist', //nume coloana
                    dataIndex: 'ArtistId', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'ArtistId',
                        displayField: 'Name',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["ArtistNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["ArtistNomStore"].findExact('ArtistId', parseInt(val));
						if(index==-1)
							index = nomStores["ArtistNomStore"].findExact('ArtistId', ""+val);
                        if (index != -1) {
                            rs = nomStores["ArtistNomStore"].getAt(index).data;
                            return rs.Name;
                        }
                    }
                },
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditalbumWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditalbumWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditalbumWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add album',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_album_AlbumId',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'AlbumId'
                }, 
				
												
				{
                    id: 'field_edit_album_Title',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Title'
                }, 
				
												
				{
                    id: 'field_edit_album_Artist',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Artist',
                    store: nomStores["ArtistNomStore"],
                    valueField: 'ArtistId',
                    displayField: 'Name',
					
                },
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _AlbumId = Ext.getCmp('field_edit_album_AlbumId').getValue();
                                    win._edit.set('AlbumId', _AlbumId);
                                    
									
																		                                    var _Title = Ext.getCmp('field_edit_album_Title').getValue();
                                    win._edit.set('Title', _Title);
                                    
									
																											
									var _ArtistId = Ext.getCmp('field_edit_album_Artist').getValue();
                                    win._edit.set('ArtistId', _ArtistId);	
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _AlbumId = Ext.getCmp('field_edit_album_AlbumId').getValue();
                                    
																											
                                    var _Title = Ext.getCmp('field_edit_album_Title').getValue();
                                    
																											var _ArtistId = Ext.getCmp('field_edit_album_Artist').getValue();
									if(_ArtistId==0) _ArtistId=null;
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        Title: _Title,
                                        
																														
                                        ArtistId: _ArtistId,
                                        
																				
                                    }, 'MyApp.model.albumModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_album_AlbumId').setValue(this._edit.getData().AlbumId);
                                                            Ext.getCmp('field_edit_album_Title').setValue(this._edit.getData().Title);
                                        					Ext.getCmp('field_edit_album_Artist').setValue(this._edit.getData().ArtistId);
					                    					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
																'ArtistModel',
										
                'albumModel',
                
				
				
            ],
            stores: [
																'ArtistNomStore',
									
                'albumStore',
                
            ],
            views: [
                'albumsTable',
                'EditalbumWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.albumsTable', {
                    renderTo: "albumsDiv"
                });
            }

        });
    }
</script>

<div id="albumsDiv">
</div>
<script>
    init();
</script>
