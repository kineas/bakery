<div class="albums view">
<h2><?php echo __('Album'); ?></h2>
	<dl>
		<dt><?php echo __('AlbumId'); ?></dt>
		<dd>
			<?php echo h($album['Album']['AlbumId']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($album['Album']['Title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ArtistId'); ?></dt>
		<dd>
			<?php echo h($album['Album']['ArtistId']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Album'), array('action' => 'edit', $album['Album']['AlbumId'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Album'), array('action' => 'delete', $album['Album']['AlbumId']), array(), __('Are you sure you want to delete # %s?', $album['Album']['AlbumId'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Albums'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Album'), array('action' => 'add')); ?> </li>
	</ul>
</div>
