<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlplaylisttracks(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Playlisttrack."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.playlisttrackModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'PlaylistId'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'TrackId'
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.TrackModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'TrackId'
			},
			 
			{                   
				name: 'Name'
			},
			 
			{                   
				name: 'AlbumId'
			},
			 
			{                   
				name: 'MediaTypeId'
			},
			 
			{                   
				name: 'GenreId'
			},
			 
			{                   
				name: 'Composer'
			},
			 
			{                   
				name: 'Milliseconds'
			},
			 
			{                   
				name: 'Bytes'
			},
			 
			{                   
				name: 'UnitPrice'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.TrackNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.TrackModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'TrackNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.TrackModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Tracks/extdataall',
                        type: 'jsonp',
                        idParam: 'TrackId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Tracks/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["TrackNomStore"] = Ext.create('MyApp.store.TrackNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.playlisttrackStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.playlisttrackModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'playlisttrackStore', // nume tabela
                    model: 'MyApp.model.playlisttrackModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'TrackId', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlplaylisttracks(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].TrackId;
                } else {
                    id = request.jsonData.TrackId;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.playlisttrackStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.playlisttracksTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_PlaylistId',
							fieldLabel: 'PlaylistId'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_TrackId',
							fieldLabel: 'Track',
							store: nomStores["TrackNomStore"],
							valueField: 'TrackId',
							displayField: 'Name',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_PlaylistId').getValue();
                                if (_val) {
                                    filter.PlaylistId = _val;
																				filter_op.PlaylistId = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_TrackId').getValue();
                                if (_val) {
                                    filter.TrackId = _val;
																			filter_op.TrackId = "=";
										                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_PlaylistId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_TrackId').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
												{
                    header: 'PlaylistId', //nume coloana
                    dataIndex: 'PlaylistId', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												 
				{
                    id: 'TrackId', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'TrackId', //nume coloana
                    text: 'TrackId' //nume coloana
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditplaylisttrackWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditplaylisttrackWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditplaylisttrackWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add playlisttrack',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_playlisttrack_PlaylistId',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'PlaylistId'
                }, 
				
												
				{
                    id: 'field_edit_playlisttrack_Track',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Track',
                    store: nomStores["TrackNomStore"],
                    valueField: 'TrackId',
                    displayField: 'Name',
					
                },
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _PlaylistId = Ext.getCmp('field_edit_playlisttrack_PlaylistId').getValue();
                                    win._edit.set('PlaylistId', _PlaylistId);
                                    
									
																											
									var _TrackId = Ext.getCmp('field_edit_playlisttrack_Track').getValue();
                                    win._edit.set('TrackId', _TrackId);	
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _PlaylistId = Ext.getCmp('field_edit_playlisttrack_PlaylistId').getValue();
                                    
																											var _TrackId = Ext.getCmp('field_edit_playlisttrack_Track').getValue();
									if(_TrackId==0) _TrackId=null;
																											
                                    var r = Ext.ModelManager.create({
										
																														
                                        PlaylistId: _PlaylistId,
                                        
																														
                                    }, 'MyApp.model.playlisttrackModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_playlisttrack_PlaylistId').setValue(this._edit.getData().PlaylistId);
                                        					Ext.getCmp('field_edit_playlisttrack_Track').setValue(this._edit.getData().TrackId);
					                    					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
												'TrackModel',
										
                'playlisttrackModel',
                
				
				
            ],
            stores: [
												'TrackNomStore',
									
                'playlisttrackStore',
                
            ],
            views: [
                'playlisttracksTable',
                'EditplaylisttrackWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.playlisttracksTable', {
                    renderTo: "playlisttracksDiv"
                });
            }

        });
    }
</script>

<div id="playlisttracksDiv">
</div>
<script>
    init();
</script>
