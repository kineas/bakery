<?php echo $this->request->query['_callback']; ?>({
	"_success":true,
	"_total" :"<?php echo $this->Paginator->counter(array('format' => __('{:count}'))); ?>", 
	"_message":"Loaded data",	
	"_data":[
    <?php foreach ($genres as $genre): ?>
			{"GenreId":<?php if($genre['Genre']['GenreId']) {echo $genre['Genre']['GenreId'].','; } else {  echo 'null,';}?>"Name":<?php $x= json_encode($genre['Genre']['Name']); if($x!='') echo $x; else echo '""';?>},	
		
<?php endforeach; ?>	]
});
