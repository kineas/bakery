<?php echo $this->request->query['_callback']; ?>({
	"_success":true,
	"_total" :"<?php echo $this->Paginator->counter(array('format' => __('{:count}'))); ?>", 
	"_message":"Loaded data",	
	"_data":[
    <?php foreach ($invoices as $invoice): ?>
			{"InvoiceId":<?php if($invoice['Invoice']['InvoiceId']) {echo $invoice['Invoice']['InvoiceId'].','; } else {  echo 'null,';}?>"CustomerId":<?php if($invoice['Invoice']['CustomerId']) {echo $invoice['Invoice']['CustomerId'].','; } else {  echo 'null,';}?>"InvoiceDate":<?php $x= json_encode($invoice['Invoice']['InvoiceDate']); if($x!='') echo $x; else echo '""';?>,"BillingAddress":<?php $x= json_encode($invoice['Invoice']['BillingAddress']); if($x!='') echo $x; else echo '""';?>,"BillingCity":<?php $x= json_encode($invoice['Invoice']['BillingCity']); if($x!='') echo $x; else echo '""';?>,"BillingState":<?php $x= json_encode($invoice['Invoice']['BillingState']); if($x!='') echo $x; else echo '""';?>,"BillingCountry":<?php $x= json_encode($invoice['Invoice']['BillingCountry']); if($x!='') echo $x; else echo '""';?>,"BillingPostalCode":<?php $x= json_encode($invoice['Invoice']['BillingPostalCode']); if($x!='') echo $x; else echo '""';?>,"Total":<?php $x= json_encode($invoice['Invoice']['Total']); if($x!='') echo $x; else echo '""';?>},	
		
<?php endforeach; ?>	]
});
