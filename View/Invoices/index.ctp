<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlinvoices(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":Invoice."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.invoiceModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'InvoiceId'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'CustomerId'
			},
			   
			 
			{                   
				name: 'InvoiceDate',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'BillingAddress',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'BillingCity',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'BillingState',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'BillingCountry',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'BillingPostalCode',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'Total',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.CustomerModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'CustomerId'
			},
			 
			{                   
				name: 'FirstName'
			},
			 
			{                   
				name: 'LastName'
			},
			 
			{                   
				name: 'Company'
			},
			 
			{                   
				name: 'Address'
			},
			 
			{                   
				name: 'City'
			},
			 
			{                   
				name: 'State'
			},
			 
			{                   
				name: 'Country'
			},
			 
			{                   
				name: 'PostalCode'
			},
			 
			{                   
				name: 'Phone'
			},
			 
			{                   
				name: 'Fax'
			},
			 
			{                   
				name: 'Email'
			},
			 
			{                   
				name: 'SupportRepId'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.CustomerNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.CustomerModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'CustomerNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.CustomerModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Customers/extdataall',
                        type: 'jsonp',
                        idParam: 'CustomerId', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Customers/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["CustomerNomStore"] = Ext.create('MyApp.store.CustomerNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.invoiceStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.invoiceModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'invoiceStore', // nume tabela
                    model: 'MyApp.model.invoiceModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'InvoiceId', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlinvoices(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].InvoiceId;
                } else {
                    id = request.jsonData.InvoiceId;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.invoiceStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.invoicesTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_InvoiceId',
							fieldLabel: 'InvoiceId'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_CustomerId',
							fieldLabel: 'Customer',
							store: nomStores["CustomerNomStore"],
							valueField: 'CustomerId',
							displayField: 'FirstName',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_InvoiceDate',
							fieldLabel: 'InvoiceDate'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_BillingAddress',
							fieldLabel: 'BillingAddress'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_BillingCity',
							fieldLabel: 'BillingCity'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_BillingState',
							fieldLabel: 'BillingState'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_BillingCountry',
							fieldLabel: 'BillingCountry'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_BillingPostalCode',
							fieldLabel: 'BillingPostalCode'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_Total',
							fieldLabel: 'Total'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_InvoiceId').getValue();
                                if (_val) {
                                    filter.InvoiceId = _val;
																			filter_op.InvoiceId = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_CustomerId').getValue();
                                if (_val) {
                                    filter.CustomerId = _val;
																				filter_op.CustomerId = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_InvoiceDate').getValue();
                                if (_val) {
                                    filter.InvoiceDate = _val;
																				filter_op.InvoiceDate = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_BillingAddress').getValue();
                                if (_val) {
                                    filter.BillingAddress = _val;
																				filter_op.BillingAddress = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_BillingCity').getValue();
                                if (_val) {
                                    filter.BillingCity = _val;
																				filter_op.BillingCity = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_BillingState').getValue();
                                if (_val) {
                                    filter.BillingState = _val;
																				filter_op.BillingState = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_BillingCountry').getValue();
                                if (_val) {
                                    filter.BillingCountry = _val;
																				filter_op.BillingCountry = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_BillingPostalCode').getValue();
                                if (_val) {
                                    filter.BillingPostalCode = _val;
																				filter_op.BillingPostalCode = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_Total').getValue();
                                if (_val) {
                                    filter.Total = _val;
																				filter_op.Total = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_InvoiceId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_CustomerId').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_InvoiceDate').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_BillingAddress').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_BillingCity').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_BillingState').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_BillingCountry').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_BillingPostalCode').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_Total').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'InvoiceId', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'InvoiceId', //nume coloana
                    text: 'InvoiceId' //nume coloana
                }, 
																
				{
                    header: 'Customer', //nume coloana
                    dataIndex: 'CustomerId', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'CustomerId',
                        displayField: 'FirstName',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["CustomerNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["CustomerNomStore"].findExact('CustomerId', parseInt(val));
						if(index==-1)
							index = nomStores["CustomerNomStore"].findExact('CustomerId', ""+val);
                        if (index != -1) {
                            rs = nomStores["CustomerNomStore"].getAt(index).data;
                            return rs.FirstName;
                        }
                    }
                },
																{
                    header: 'InvoiceDate', //nume coloana
                    dataIndex: 'InvoiceDate', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'BillingAddress', //nume coloana
                    dataIndex: 'BillingAddress', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'BillingCity', //nume coloana
                    dataIndex: 'BillingCity', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'BillingState', //nume coloana
                    dataIndex: 'BillingState', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'BillingCountry', //nume coloana
                    dataIndex: 'BillingCountry', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'BillingPostalCode', //nume coloana
                    dataIndex: 'BillingPostalCode', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'Total', //nume coloana
                    dataIndex: 'Total', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditinvoiceWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditinvoiceWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditinvoiceWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add invoice',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_invoice_InvoiceId',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'InvoiceId'
                }, 
				
												
				{
                    id: 'field_edit_invoice_Customer',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Customer',
                    store: nomStores["CustomerNomStore"],
                    valueField: 'CustomerId',
                    displayField: 'FirstName',
					
                },
												
				{
                    id: 'field_edit_invoice_InvoiceDate',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'InvoiceDate'
                }, 
				
												
				{
                    id: 'field_edit_invoice_BillingAddress',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'BillingAddress'
                }, 
				
												
				{
                    id: 'field_edit_invoice_BillingCity',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'BillingCity'
                }, 
				
												
				{
                    id: 'field_edit_invoice_BillingState',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'BillingState'
                }, 
				
												
				{
                    id: 'field_edit_invoice_BillingCountry',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'BillingCountry'
                }, 
				
												
				{
                    id: 'field_edit_invoice_BillingPostalCode',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'BillingPostalCode'
                }, 
				
												
				{
                    id: 'field_edit_invoice_Total',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'Total'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _InvoiceId = Ext.getCmp('field_edit_invoice_InvoiceId').getValue();
                                    win._edit.set('InvoiceId', _InvoiceId);
                                    
									
																											
									var _CustomerId = Ext.getCmp('field_edit_invoice_Customer').getValue();
                                    win._edit.set('CustomerId', _CustomerId);	
																		                                    var _InvoiceDate = Ext.getCmp('field_edit_invoice_InvoiceDate').getValue();
                                    win._edit.set('InvoiceDate', _InvoiceDate);
                                    
									
																		                                    var _BillingAddress = Ext.getCmp('field_edit_invoice_BillingAddress').getValue();
                                    win._edit.set('BillingAddress', _BillingAddress);
                                    
									
																		                                    var _BillingCity = Ext.getCmp('field_edit_invoice_BillingCity').getValue();
                                    win._edit.set('BillingCity', _BillingCity);
                                    
									
																		                                    var _BillingState = Ext.getCmp('field_edit_invoice_BillingState').getValue();
                                    win._edit.set('BillingState', _BillingState);
                                    
									
																		                                    var _BillingCountry = Ext.getCmp('field_edit_invoice_BillingCountry').getValue();
                                    win._edit.set('BillingCountry', _BillingCountry);
                                    
									
																		                                    var _BillingPostalCode = Ext.getCmp('field_edit_invoice_BillingPostalCode').getValue();
                                    win._edit.set('BillingPostalCode', _BillingPostalCode);
                                    
									
																		                                    var _Total = Ext.getCmp('field_edit_invoice_Total').getValue();
                                    win._edit.set('Total', _Total);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _InvoiceId = Ext.getCmp('field_edit_invoice_InvoiceId').getValue();
                                    
																											var _CustomerId = Ext.getCmp('field_edit_invoice_Customer').getValue();
									if(_CustomerId==0) _CustomerId=null;
																											
                                    var _InvoiceDate = Ext.getCmp('field_edit_invoice_InvoiceDate').getValue();
                                    
																											
                                    var _BillingAddress = Ext.getCmp('field_edit_invoice_BillingAddress').getValue();
                                    
																											
                                    var _BillingCity = Ext.getCmp('field_edit_invoice_BillingCity').getValue();
                                    
																											
                                    var _BillingState = Ext.getCmp('field_edit_invoice_BillingState').getValue();
                                    
																											
                                    var _BillingCountry = Ext.getCmp('field_edit_invoice_BillingCountry').getValue();
                                    
																											
                                    var _BillingPostalCode = Ext.getCmp('field_edit_invoice_BillingPostalCode').getValue();
                                    
																											
                                    var _Total = Ext.getCmp('field_edit_invoice_Total').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        CustomerId: _CustomerId,
                                        
																														
                                        InvoiceDate: _InvoiceDate,
                                        
																														
                                        BillingAddress: _BillingAddress,
                                        
																														
                                        BillingCity: _BillingCity,
                                        
																														
                                        BillingState: _BillingState,
                                        
																														
                                        BillingCountry: _BillingCountry,
                                        
																														
                                        BillingPostalCode: _BillingPostalCode,
                                        
																														
                                        Total: _Total,
                                        
																				
                                    }, 'MyApp.model.invoiceModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_invoice_InvoiceId').setValue(this._edit.getData().InvoiceId);
                                        					Ext.getCmp('field_edit_invoice_Customer').setValue(this._edit.getData().CustomerId);
					                                        Ext.getCmp('field_edit_invoice_InvoiceDate').setValue(this._edit.getData().InvoiceDate);
                                                            Ext.getCmp('field_edit_invoice_BillingAddress').setValue(this._edit.getData().BillingAddress);
                                                            Ext.getCmp('field_edit_invoice_BillingCity').setValue(this._edit.getData().BillingCity);
                                                            Ext.getCmp('field_edit_invoice_BillingState').setValue(this._edit.getData().BillingState);
                                                            Ext.getCmp('field_edit_invoice_BillingCountry').setValue(this._edit.getData().BillingCountry);
                                                            Ext.getCmp('field_edit_invoice_BillingPostalCode').setValue(this._edit.getData().BillingPostalCode);
                                                            Ext.getCmp('field_edit_invoice_Total').setValue(this._edit.getData().Total);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
												'CustomerModel',
																																						
                'invoiceModel',
                
				
				
            ],
            stores: [
												'CustomerNomStore',
																																					
                'invoiceStore',
                
            ],
            views: [
                'invoicesTable',
                'EditinvoiceWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.invoicesTable', {
                    renderTo: "invoicesDiv"
                });
            }

        });
    }
</script>

<div id="invoicesDiv">
</div>
<script>
    init();
</script>
