<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlarosAcos(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":ArosAco."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.arosAcoModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'id'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'aro_id'
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'aco_id'
			},
			   
			 
			{                   
				name: '_create',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: '_read',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: '_update',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: '_delete',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.AroModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'id'
			},
			 
			{                   
				name: 'parent_id'
			},
			 
			{                   
				name: 'model'
			},
			 
			{                   
				name: 'foreign_key'
			},
			 
			{                   
				name: 'alias'
			},
			 
			{                   
				name: 'lft'
			},
			 
			{                   
				name: 'rght'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.AroNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.AroModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'AroNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.AroModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Aros/extdataall',
                        type: 'jsonp',
                        idParam: 'id', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Aros/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["AroNomStore"] = Ext.create('MyApp.store.AroNomStore');
				
		
        Ext.define('MyApp.model.AcoModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'id'
			},
			 
			{                   
				name: 'parent_id'
			},
			 
			{                   
				name: 'model'
			},
			 
			{                   
				name: 'foreign_key'
			},
			 
			{                   
				name: 'alias'
			},
			 
			{                   
				name: 'lft'
			},
			 
			{                   
				name: 'rght'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.AcoNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.AcoModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'AcoNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.AcoModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Acos/extdataall',
                        type: 'jsonp',
                        idParam: 'id', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Acos/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["AcoNomStore"] = Ext.create('MyApp.store.AcoNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.arosAcoStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.arosAcoModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'arosAcoStore', // nume tabela
                    model: 'MyApp.model.arosAcoModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'id', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlarosAcos(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].id;
                } else {
                    id = request.jsonData.id;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.arosAcoStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.arosAcosTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_id',
							fieldLabel: 'id'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_id',
							fieldLabel: 'Aro',
							store: nomStores["AroNomStore"],
							valueField: 'id',
							displayField: 'alias',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_id',
							fieldLabel: 'Aco',
							store: nomStores["AcoNomStore"],
							valueField: 'id',
							displayField: 'alias',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter__create',
							fieldLabel: '_create'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter__read',
							fieldLabel: '_read'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter__update',
							fieldLabel: '_update'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter__delete',
							fieldLabel: '_delete'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_id').getValue();
                                if (_val) {
                                    filter.id = _val;
																			filter_op.id = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_aro_id').getValue();
                                if (_val) {
                                    filter.aro_id = _val;
																				filter_op.aro_id = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_aco_id').getValue();
                                if (_val) {
                                    filter.aco_id = _val;
																				filter_op.aco_id = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter__create').getValue();
                                if (_val) {
                                    filter._create = _val;
																				filter_op._create = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter__read').getValue();
                                if (_val) {
                                    filter._read = _val;
																				filter_op._read = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter__update').getValue();
                                if (_val) {
                                    filter._update = _val;
																				filter_op._update = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter__delete').getValue();
                                if (_val) {
                                    filter._delete = _val;
																				filter_op._delete = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_aro_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_aco_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter__create').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter__read').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter__update').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter__delete').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'id', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'id', //nume coloana
                    text: 'id' //nume coloana
                }, 
																
				{
                    header: 'Aro', //nume coloana
                    dataIndex: 'id', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'id',
                        displayField: 'alias',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["AroNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["AroNomStore"].findExact('id', parseInt(val));
						if(index==-1)
							index = nomStores["AroNomStore"].findExact('id', ""+val);
                        if (index != -1) {
                            rs = nomStores["AroNomStore"].getAt(index).data;
                            return rs.alias;
                        }
                    }
                },
																
				{
                    header: 'Aco', //nume coloana
                    dataIndex: 'id', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'id',
                        displayField: 'alias',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["AcoNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["AcoNomStore"].findExact('id', parseInt(val));
						if(index==-1)
							index = nomStores["AcoNomStore"].findExact('id', ""+val);
                        if (index != -1) {
                            rs = nomStores["AcoNomStore"].getAt(index).data;
                            return rs.alias;
                        }
                    }
                },
																{
                    header: '_create', //nume coloana
                    dataIndex: '_create', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: '_read', //nume coloana
                    dataIndex: '_read', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: '_update', //nume coloana
                    dataIndex: '_update', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: '_delete', //nume coloana
                    dataIndex: '_delete', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EditarosAcoWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EditarosAcoWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EditarosAcoWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add arosAco',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_arosAco_id',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'id'
                }, 
				
												
				{
                    id: 'field_edit_arosAco_Aro',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Aro',
                    store: nomStores["AroNomStore"],
                    valueField: 'id',
                    displayField: 'alias',
					
                },
												
				{
                    id: 'field_edit_arosAco_Aco',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Aco',
                    store: nomStores["AcoNomStore"],
                    valueField: 'id',
                    displayField: 'alias',
					
                },
												
				{
                    id: 'field_edit_arosAco__create',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: '_create'
                }, 
				
												
				{
                    id: 'field_edit_arosAco__read',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: '_read'
                }, 
				
												
				{
                    id: 'field_edit_arosAco__update',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: '_update'
                }, 
				
												
				{
                    id: 'field_edit_arosAco__delete',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: '_delete'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _id = Ext.getCmp('field_edit_arosAco_id').getValue();
                                    win._edit.set('id', _id);
                                    
									
																											
									var _aro_id = Ext.getCmp('field_edit_arosAco_Aro').getValue();
                                    win._edit.set('id', _id);	
																											
									var _aco_id = Ext.getCmp('field_edit_arosAco_Aco').getValue();
                                    win._edit.set('id', _id);	
																		                                    var __create = Ext.getCmp('field_edit_arosAco__create').getValue();
                                    win._edit.set('_create', __create);
                                    
									
																		                                    var __read = Ext.getCmp('field_edit_arosAco__read').getValue();
                                    win._edit.set('_read', __read);
                                    
									
																		                                    var __update = Ext.getCmp('field_edit_arosAco__update').getValue();
                                    win._edit.set('_update', __update);
                                    
									
																		                                    var __delete = Ext.getCmp('field_edit_arosAco__delete').getValue();
                                    win._edit.set('_delete', __delete);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _id = Ext.getCmp('field_edit_arosAco_id').getValue();
                                    
																											var _aro_id = Ext.getCmp('field_edit_arosAco_Aro').getValue();
									if(_aro_id==0) _aro_id=null;
																											var _aco_id = Ext.getCmp('field_edit_arosAco_Aco').getValue();
									if(_aco_id==0) _aco_id=null;
																											
                                    var __create = Ext.getCmp('field_edit_arosAco__create').getValue();
                                    
																											
                                    var __read = Ext.getCmp('field_edit_arosAco__read').getValue();
                                    
																											
                                    var __update = Ext.getCmp('field_edit_arosAco__update').getValue();
                                    
																											
                                    var __delete = Ext.getCmp('field_edit_arosAco__delete').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        aro_id: _aro_id,
                                        
																														
                                        aco_id: _aco_id,
                                        
																														
                                        _create: __create,
                                        
																														
                                        _read: __read,
                                        
																														
                                        _update: __update,
                                        
																														
                                        _delete: __delete,
                                        
																				
                                    }, 'MyApp.model.arosAcoModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_arosAco_id').setValue(this._edit.getData().id);
                                        					Ext.getCmp('field_edit_arosAco_Aro').setValue(this._edit.getData().id);
					                    					Ext.getCmp('field_edit_arosAco_Aco').setValue(this._edit.getData().id);
					                                        Ext.getCmp('field_edit_arosAco__create').setValue(this._edit.getData()._create);
                                                            Ext.getCmp('field_edit_arosAco__read').setValue(this._edit.getData()._read);
                                                            Ext.getCmp('field_edit_arosAco__update').setValue(this._edit.getData()._update);
                                                            Ext.getCmp('field_edit_arosAco__delete').setValue(this._edit.getData()._delete);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
												'AroModel',
												'AcoModel',
																										
                'arosAcoModel',
                
				
				
            ],
            stores: [
												'AroNomStore',
												'AcoNomStore',
																									
                'arosAcoStore',
                
            ],
            views: [
                'arosAcosTable',
                'EditarosAcoWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.arosAcosTable', {
                    renderTo: "arosAcosDiv"
                });
            }

        });
    }
</script>

<div id="arosAcosDiv">
</div>
<script>
    init();
</script>
