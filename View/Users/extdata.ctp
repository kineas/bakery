<?php echo $this->request->query['_callback']; ?>({
	"_success":true,
	"_total" :"<?php echo $this->Paginator->counter(array('format' => __('{:count}'))); ?>", 
	"_message":"Loaded data",	
	"_data":[
    <?php foreach ($users as $user): ?>
			{"id":<?php if($user['User']['id']) {echo $user['User']['id'].','; } else {  echo 'null,';}?>"username":<?php $x= json_encode($user['User']['username']); if($x!='') echo $x; else echo '""';?>,"firstname":<?php $x= json_encode($user['User']['firstname']); if($x!='') echo $x; else echo '""';?>,"lastname":<?php $x= json_encode($user['User']['lastname']); if($x!='') echo $x; else echo '""';?>,"email":<?php $x= json_encode($user['User']['email']); if($x!='') echo $x; else echo '""';?>,"department":<?php $x= json_encode($user['User']['department']); if($x!='') echo $x; else echo '""';?>,"password":<?php $x= json_encode($user['User']['password']); if($x!='') echo $x; else echo '""';?>,"group_id":<?php if($user['User']['group_id']) {echo $user['User']['group_id'].','; } else {  echo 'null,';}?>"created":<?php $x= json_encode($user['User']['created']); if($x!='') echo $x; else echo '""';?>,"modified":<?php $x= json_encode($user['User']['modified']); if($x!='') echo $x; else echo '""';?>},	
		
<?php endforeach; ?>	]
});
