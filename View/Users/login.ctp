<div class="users form">
<?php echo $this->Form->create() ?>
    <fieldset>
        <legend><?php echo 'Please enter your username and password'; ?></legend>
        <?php echo $this->Form->input('username') ?>
        <?php echo $this->Form->input('password') ?>
    </fieldset>
<?php echo $this->Form->button(__('Login')); ?>
<?php echo $this->Form->end() ?>
</div>