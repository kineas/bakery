<script>
    var nomStores = {};
    var nomCombo = {};
    var mainStore;
    var mainView;
    var filter = {};
	var filter_op = {};

    function urlusers(request) { //functie de definire a url-request
        var url = "";
        if (request.action == "read") {
            var sort = eval(request.params.sort);
            var sort_text = "";
            var page_text = "";
            if (request.params.page) {
                page_text = "/page:" + request.params.page;
            }
            if (sort) {
                sort_text = "/sort:" + sort[0].property + "/direction:" + sort[0].direction;
            }
            url = request.proxy.crud.index + page_text + sort_text;
			i=0;
			if(filter!={})
			{
				for(var propertyName in filter) {
				   var value = filter[propertyName];
				   if(value!=null && value!="" && value!=undefined){
						i++;
						url+="/filter.field.filter"+i+":User."+propertyName;
						url+="/filter.operator.filter"+i+":"+filter_op[propertyName];
						url+="/filter.filter"+i+":"+value;
				   }
				}
			}
            return url;
        }

    }

    function defineMainModel() {
        Ext.define('MyApp.model.userModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [   //definirea campurile in model !!
			
			
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'id'
			},
			   
			 
			{                   
				name: 'username',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'firstname',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'lastname',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'email',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'department',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'password',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{       
				 type: 'int',
				 useNull: true,
				name: 'group_id'
			},
			   
			 
			{                   
				name: 'created',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
			 
			{                   
				name: 'modified',
				allowNull: true,
				validations: [
								{
										type: 'length',
										min: 5
								}
						]
			},
			   
						
			]
        });
    }



    function defineNoms() {  //defineste norms pentru toate campurile de genul belongsTo
				
		
        Ext.define('MyApp.model.GroupModel', {
            extend: 'Ext.data.Model',
            requires: [
                // 'Ext.data.field.Boolean',
                // 'Ext.data.field.Date',
                // 'Ext.data.field.Number',
                // 'Ext.data.field.String'
            ],
            fields: [
			
			 
			{                   
				name: 'id'
			},
			 
			{                   
				name: 'name'
			},
			 
			{                   
				name: 'created'
			},
			 
			{                   
				name: 'modified'
			},
			//end foreach 			
			]
        });
        Ext.define('MyApp.store.GroupNomStore', {  //storul pentru belongs to
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.GroupModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    storeId: 'GroupNomStore', // nume tabela nomenclator
                    model: 'MyApp.model.GroupModel', // nume tabela nomenclator
                    autoLoad: true,
                    proxy: {
                        url: '../Groups/extdataall',
                        type: 'jsonp',
                        idParam: 'id', // nume id nomenclator                         
                        recordParam: '_records',
                        callbackKey: '_callback',
                        crud: {
                            index: '../Groups/extdataall'
                        },
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type'
                        }
                    }
                }, cfg)]);
            }
        });
        nomStores["GroupNomStore"] = Ext.create('MyApp.store.GroupNomStore');
		 //end for	
    }

    function defineMainStore() {
        Ext.define('MyApp.store.userStore', {
            extend: 'Ext.data.Store',
            requires: [
                'MyApp.model.userModel',
                'Ext.data.proxy.JsonP',
                'Ext.data.reader.Json',
                'Ext.data.writer.Json'
            ],
            constructor: function(cfg) {
                var me = this;
                cfg = cfg || {};
                me.callParent([Ext.apply({
                    pageSize: 20,
                    remoteFilter: true,
                    remoteSort: true,
                    storeId: 'userStore', // nume tabela
                    model: 'MyApp.model.userModel', // nume tabela
                    autoLoad: true,
                    autoSync: true,
                    proxy: {
                        url: 'extdata',
                        api: {
                            // The *.action is important
                            read: 'extdata',
                            create: 'add{%id%}',
                            update: 'edit{%id%}',
                            destroy: 'delete{%id%}'
                        },
                        actionMethods: {
                            create: 'POST',
                            update: 'POST',
                            destroy: 'POST'
                        },
                        crud: {
                            index: "extdata",
                            view: "view",
                            update: "edit",
                            insert: "add",
                            delete: "delete"
                        },
                        type: 'jsonp',
                        cacheString: '_cachString',
                        directionParam: '_dir',
                        filterParam: '_filter',
                        groupDirectionParam: '_groupDir',
                        groupParam: '_group',
                        idParam: 'id', // nume id
                        limitParam: 'limit',
                        sortParam: 'sort',
                        startParam: 'start',
                        callbackKey: '_callback',
                        recordParam: '_records',
                        //buildUrl: function (request) { return urlAlbums(request,this);},
                        reader: {
                            type: 'json',
                            messageProperty: '_message',
                            rootProperty: '_root',
                            root: "_data",
                            successProperty: '_success',
                            totalProperty: '_total',
                            typeProperty: '_type',
                            // record: '_record'
                        },
                        writer: {
                            type: 'json',
                            clientIdProperty: '_clientId',
                            nameProperty: '_name',
                            writeAllFields: true,
                            expandData: true
                        }
                    }
                }, cfg)]);
            }
        });

        Ext.override(Ext.data.proxy.JsonP, {
            buildUrl: function(request) {
                var url = this.callParent(arguments);
                console.log(url);
                if (request.action == "read") {
                    return urlusers(request);
                }
                var id = 0;
                if (Object.prototype.toString.call(request.jsonData) === '[object Array]') {
                    id = request.jsonData[request.jsonData.length - 1].id;
                } else {
                    id = request.jsonData.id;
                }
                if (request.jsonData)
                    url = url.replace('{%id%}', "/" + id);
                return url;
            }
        });

        mainStore = Ext.create('MyApp.store.userStore');

    }

    function defineMainGrid() {
        Ext.define('MyApp.view.usersTable', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.mypanel',

            requires: [
                'Ext.form.field.ComboBox',
                'Ext.form.field.Checkbox',
                'Ext.form.field.Date',
                'Ext.form.field.Number',
                'Ext.grid.Panel',
                'Ext.grid.column.Number',
                'Ext.view.Table',
                'Ext.toolbar.Paging',
                'Ext.grid.plugin.CellEditing',
                'Ext.button.Button'
            ],
            items: [{
                xtype: 'container',
                layout: 'form',
				width: "400px",
                items: [
				
					 //end if	
						{
							xtype: 'textfield',
							id: 'filter_id',
							fieldLabel: 'id'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_username',
							fieldLabel: 'username'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_firstname',
							fieldLabel: 'firstname'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_lastname',
							fieldLabel: 'lastname'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_email',
							fieldLabel: 'email'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_department',
							fieldLabel: 'department'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_password',
							fieldLabel: 'password'
						}, 
							 //end else
						 
						{
							xtype: 'combobox',
							id: 'filter_id',
							fieldLabel: 'Group',
							store: nomStores["GroupNomStore"],
							valueField: 'id',
							displayField: 'name',
							forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
							
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_created',
							fieldLabel: 'created'
						}, 
							 //end else
						 //end if	
						{
							xtype: 'textfield',
							id: 'filter_modified',
							fieldLabel: 'modified'
						}, 
							 //end else
						 //end for		
					
					
					{
                        xtype: 'panel',
                        items: [
						
							{
                            xtype: 'button',
                            text: 'Search',
                            listeners: {
                                click: 'onButtonClick'
                            },
							
                            onButtonClick: function(button, e, eOpts) {
								
                                
								
                                filter = {};
								 filter_op = {};
								                                var _val = Ext.getCmp('filter_id').getValue();
                                if (_val) {
                                    filter.id = _val;
																			filter_op.id = "=";
										                                }
								
								
								                                var _val = Ext.getCmp('filter_username').getValue();
                                if (_val) {
                                    filter.username = _val;
																				filter_op.username = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_firstname').getValue();
                                if (_val) {
                                    filter.firstname = _val;
																				filter_op.firstname = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_lastname').getValue();
                                if (_val) {
                                    filter.lastname = _val;
																				filter_op.lastname = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_email').getValue();
                                if (_val) {
                                    filter.email = _val;
																				filter_op.email = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_department').getValue();
                                if (_val) {
                                    filter.department = _val;
																				filter_op.department = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_password').getValue();
                                if (_val) {
                                    filter.password = _val;
																				filter_op.password = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_group_id').getValue();
                                if (_val) {
                                    filter.group_id = _val;
																				filter_op.group_id = "=";
											
											                                }
								
								
								                                var _val = Ext.getCmp('filter_created').getValue();
                                if (_val) {
                                    filter.created = _val;
																				filter_op.created = "LIKE";
											                                }
								
								
								                                var _val = Ext.getCmp('filter_modified').getValue();
                                if (_val) {
                                    filter.modified = _val;
																				filter_op.modified = "LIKE";
											                                }
								
								
																mainStore.reload();
								
								
								
                            }
                        },

						{
                            xtype: 'button',
                            text: 'Clear',
                            listeners: {
                                click: 'onButtonClick'
                            },
                            onButtonClick: function(button, e, eOpts) {
																
                                Ext.getCmp('filter_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_username').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_firstname').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_lastname').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_email').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_department').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_password').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_group_id').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_created').setValue('');
                                
                                
								
																
                                Ext.getCmp('filter_modified').setValue('');
                                
                                
								
																filter = {};
								 filter_op = {};
                                mainStore.reload();
                            }
                        }
						
						]
                    },



                   
                ]
            }, {
                xtype: 'gridpanel',
                title: '',
                titleCollapse: true,
                store: mainStore,

                dockedItems: [{
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    displayInfo: true,
                    store: mainStore
                }, ],
                columns: [
				//>>>>>>>>>>>>>>>>>
								 
				{
                    id: 'id', //nume coloana
                    xtype: 'numbercolumn',
                    dataIndex: 'id', //nume coloana
                    text: 'id' //nume coloana
                }, 
																{
                    header: 'username', //nume coloana
                    dataIndex: 'username', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'firstname', //nume coloana
                    dataIndex: 'firstname', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'lastname', //nume coloana
                    dataIndex: 'lastname', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'email', //nume coloana
                    dataIndex: 'email', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'department', //nume coloana
                    dataIndex: 'department', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'password', //nume coloana
                    dataIndex: 'password', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																
				{
                    header: 'Group', //nume coloana
                    dataIndex: 'id', //nume coloana					
                    width: 130,
                    editor: {
                        xtype: 'combobox',
                        typeAhead: true,
                        anyMatch: true,
                        //forceSelection: true,
                        valueField: 'id',
                        displayField: 'name',
                        triggerAction: 'all',
                        selectOnTab: true,
                        store: nomStores["GroupNomStore"],
                        //lazyRender: true,
                        listClass: 'x-combo-list-small'
                    },
                    renderer: function(val) {
                        index = nomStores["GroupNomStore"].findExact('id', parseInt(val));
						if(index==-1)
							index = nomStores["GroupNomStore"].findExact('id', ""+val);
                        if (index != -1) {
                            rs = nomStores["GroupNomStore"].getAt(index).data;
                            return rs.name;
                        }
                    }
                },
																{
                    header: 'created', //nume coloana
                    dataIndex: 'created', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
																{
                    header: 'modified', //nume coloana
                    dataIndex: 'modified', //nume coloana
                    flex: 1,
                    field: {
                        allowBlank: false
                    }
                }, 
												//<<<<<<<<<<<<<<<<<<<	
				{
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/edit41.png',
                        handler: function(grid, rowIndex, colindex) {
                            var record = mainStore.getAt(rowIndex);
                            var _window = Ext.create('MyApp.view.EdituserWindow');
                            _window._edit = record;
                            _window.show();

                        }
                    }]
                }, {
                    xtype: 'actioncolumn',
                    width: 40,
                    items: [{
                        icon: '<?php echo $this->webroot; ?>img/minus75.png',
                        handler: function(grid, rowIndex, colindex) {
                            Ext.MessageBox.confirm('Delete', 'Are you sure ?', function(btn) {
                                if (btn === 'yes') {
                                    mainStore.remove(mainStore.getAt(rowIndex));
                                } else {

                                }
                            });

                        }
                    }]
                }],
                // plugins: [
                // {
                // ptype: 'rowediting'
                // }
                // ],
                tbar: [{
                     icon: '<?php echo $this->webroot; ?>img/add133_16.png',
                    handler: function() {
                        // Create a record instance through the ModelManager
                        var _window = Ext.create('MyApp.view.EdituserWindow');
                        _window.show();

                        // var r = Ext.ModelManager.create({
                        // Title: '-',
                        // ArtistId: 1
                        // }, 'MyApp.model.albumModel');
                        // mainStore.insert(0, r);
                        // this.findParentByType("gridpanel").plugins[0].startEdit(r,1);
                    }
                }],
            }]
        });

    }



    function defineEditWindow() {
        Ext.define('MyApp.view.EdituserWindow', {
            extend: 'Ext.window.Window',
            alias: 'widget.mywindow',

            requires: [

                'Ext.form.Panel',
                'Ext.form.field.ComboBox',
                'Ext.button.Button'
            ],


           // width: 400,
           // height: 300,
           // x: 200,
           // y: 300,
            title: 'Add user',
            defaultListenerScope: true,
            modal: true,
            listeners: {
                show: 'onWindowShow'
            },
            items: [{
                xtype: 'form',
                bodyPadding: 10,
                title: '',
                items: [
				//>>>>>>>>>>>>>>>
								
				{
                    id: 'field_edit_user_id',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'id'
                }, 
				
												
				{
                    id: 'field_edit_user_username',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'username'
                }, 
				
												
				{
                    id: 'field_edit_user_firstname',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'firstname'
                }, 
				
												
				{
                    id: 'field_edit_user_lastname',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'lastname'
                }, 
				
												
				{
                    id: 'field_edit_user_email',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'email'
                }, 
				
												
				{
                    id: 'field_edit_user_department',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'department'
                }, 
				
												
				{
                    id: 'field_edit_user_password',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'password'
                }, 
				
												
				{
                    id: 'field_edit_user_Group',
                    xtype: 'combobox',
					forceSelection: false, allowBlank: true, typeAhead : true, queryMode: 'local',
                    anchor: '100%',
                    fieldLabel: 'Group',
                    store: nomStores["GroupNomStore"],
                    valueField: 'id',
                    displayField: 'name',
					
                },
												
				{
                    id: 'field_edit_user_created',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'created'
                }, 
				
												
				{
                    id: 'field_edit_user_modified',
                    xtype: 'textfield',
                    anchor: '100%',
                    fieldLabel: 'modified'
                }, 
				
												//<<<<<<<<<<<<<<<<<<<

				{
                    xtype: 'container',
                    items: [{
                        xtype: 'button',
                        text: 'OK',
                        listeners: {
                            click: function(button, e, eOpts) {
                                var win = this.findParentByType('window');
                                if (win._edit) {
									
									                                    var _id = Ext.getCmp('field_edit_user_id').getValue();
                                    win._edit.set('id', _id);
                                    
									
																		                                    var _username = Ext.getCmp('field_edit_user_username').getValue();
                                    win._edit.set('username', _username);
                                    
									
																		                                    var _firstname = Ext.getCmp('field_edit_user_firstname').getValue();
                                    win._edit.set('firstname', _firstname);
                                    
									
																		                                    var _lastname = Ext.getCmp('field_edit_user_lastname').getValue();
                                    win._edit.set('lastname', _lastname);
                                    
									
																		                                    var _email = Ext.getCmp('field_edit_user_email').getValue();
                                    win._edit.set('email', _email);
                                    
									
																		                                    var _department = Ext.getCmp('field_edit_user_department').getValue();
                                    win._edit.set('department', _department);
                                    
									
																		                                    var _password = Ext.getCmp('field_edit_user_password').getValue();
                                    win._edit.set('password', _password);
                                    
									
																											
									var _group_id = Ext.getCmp('field_edit_user_Group').getValue();
                                    win._edit.set('id', _id);	
																		                                    var _created = Ext.getCmp('field_edit_user_created').getValue();
                                    win._edit.set('created', _created);
                                    
									
																		                                    var _modified = Ext.getCmp('field_edit_user_modified').getValue();
                                    win._edit.set('modified', _modified);
                                    
									
																											
                                    console.log(win._edit.validate());
                                    win._edit.commit();
                                } else {
									
																		
                                    var _id = Ext.getCmp('field_edit_user_id').getValue();
                                    
																											
                                    var _username = Ext.getCmp('field_edit_user_username').getValue();
                                    
																											
                                    var _firstname = Ext.getCmp('field_edit_user_firstname').getValue();
                                    
																											
                                    var _lastname = Ext.getCmp('field_edit_user_lastname').getValue();
                                    
																											
                                    var _email = Ext.getCmp('field_edit_user_email').getValue();
                                    
																											
                                    var _department = Ext.getCmp('field_edit_user_department').getValue();
                                    
																											
                                    var _password = Ext.getCmp('field_edit_user_password').getValue();
                                    
																											var _group_id = Ext.getCmp('field_edit_user_Group').getValue();
									if(_group_id==0) _group_id=null;
																											
                                    var _created = Ext.getCmp('field_edit_user_created').getValue();
                                    
																											
                                    var _modified = Ext.getCmp('field_edit_user_modified').getValue();
                                    
																											
                                    var r = Ext.ModelManager.create({
										
																																								
                                        username: _username,
                                        
																														
                                        firstname: _firstname,
                                        
																														
                                        lastname: _lastname,
                                        
																														
                                        email: _email,
                                        
																														
                                        department: _department,
                                        
																														
                                        password: _password,
                                        
																														
                                        group_id: _group_id,
                                        
																														
                                        created: _created,
                                        
																														
                                        modified: _modified,
                                        
																				
                                    }, 'MyApp.model.userModel');
                                    console.log(r.validate());
                                    mainStore.insert(0, r);
                                }
                                this.findParentByType('window').destroy();
                            }
                        }
                    }, {
                        xtype: 'button',
                        text: 'Cancel',
                        listeners: {
                            click: function(button, e, eOpts) {
                                this.findParentByType('window').destroy();
                            }
                        }
                    }]
                }]
            }],
            onWindowShow: function(component, eOpts) {
                if (this._edit) {
					                    Ext.getCmp('field_edit_user_id').setValue(this._edit.getData().id);
                                                            Ext.getCmp('field_edit_user_username').setValue(this._edit.getData().username);
                                                            Ext.getCmp('field_edit_user_firstname').setValue(this._edit.getData().firstname);
                                                            Ext.getCmp('field_edit_user_lastname').setValue(this._edit.getData().lastname);
                                                            Ext.getCmp('field_edit_user_email').setValue(this._edit.getData().email);
                                                            Ext.getCmp('field_edit_user_department').setValue(this._edit.getData().department);
                                                            Ext.getCmp('field_edit_user_password').setValue(this._edit.getData().password);
                                        					Ext.getCmp('field_edit_user_Group').setValue(this._edit.getData().id);
					                                        Ext.getCmp('field_edit_user_created').setValue(this._edit.getData().created);
                                                            Ext.getCmp('field_edit_user_modified').setValue(this._edit.getData().modified);
                                        					
					
					
                }
            }
        });
    }

    function init() {
        defineMainModel();

        defineNoms();

        defineMainStore();

        defineMainGrid();

        defineEditWindow();

        Ext.application({
            models: [
																																				'GroupModel',
																		
                'userModel',
                
				
				
            ],
            stores: [
																																				'GroupNomStore',
																	
                'userStore',
                
            ],
            views: [
                'usersTable',
                'EdituserWindow'
            ],
            name: 'MyApp',
            launch: function() {
                Ext.create('MyApp.view.usersTable', {
                    renderTo: "usersDiv"
                });
            }

        });
    }
</script>

<div id="usersDiv">
</div>
<script>
    init();
</script>
