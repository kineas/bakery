<?php
/**
 * InvoicelineFixture
 *
 */
class InvoicelineFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'invoiceline';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'InvoiceLineId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'InvoiceId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'TrackId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'UnitPrice' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'Quantity' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'InvoiceLineId', 'unique' => 1),
			'IFK_InvoiceLineInvoiceId' => array('column' => 'InvoiceId', 'unique' => 0),
			'IFK_InvoiceLineTrackId' => array('column' => 'TrackId', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'InvoiceLineId' => 1,
			'InvoiceId' => 1,
			'TrackId' => 1,
			'UnitPrice' => '',
			'Quantity' => 1
		),
	);

}
