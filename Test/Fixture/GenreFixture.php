<?php
/**
 * GenreFixture
 *
 */
class GenreFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'genre';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'GenreId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'Name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 120, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'GenreId', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'GenreId' => 1,
			'Name' => 'Lorem ipsum dolor sit amet'
		),
	);

}
