<?php
/**
 * PlaylisttrackFixture
 *
 */
class PlaylisttrackFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'playlisttrack';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'PlaylistId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'TrackId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('PlaylistId', 'TrackId'), 'unique' => 1),
			'IFK_PlaylistTrackTrackId' => array('column' => 'TrackId', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'PlaylistId' => 1,
			'TrackId' => 1
		),
	);

}
