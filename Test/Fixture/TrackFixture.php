<?php
/**
 * TrackFixture
 *
 */
class TrackFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'track';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'TrackId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'Name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'AlbumId' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'MediaTypeId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'GenreId' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'Composer' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 220, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Milliseconds' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'Bytes' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'UnitPrice' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'TrackId', 'unique' => 1),
			'IFK_TrackAlbumId' => array('column' => 'AlbumId', 'unique' => 0),
			'IFK_TrackGenreId' => array('column' => 'GenreId', 'unique' => 0),
			'IFK_TrackMediaTypeId' => array('column' => 'MediaTypeId', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'TrackId' => 1,
			'Name' => 'Lorem ipsum dolor sit amet',
			'AlbumId' => 1,
			'MediaTypeId' => 1,
			'GenreId' => 1,
			'Composer' => 'Lorem ipsum dolor sit amet',
			'Milliseconds' => 1,
			'Bytes' => 1,
			'UnitPrice' => ''
		),
	);

}
