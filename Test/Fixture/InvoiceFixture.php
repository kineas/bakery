<?php
/**
 * InvoiceFixture
 *
 */
class InvoiceFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'invoice';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'InvoiceId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'CustomerId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'InvoiceDate' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'BillingAddress' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 70, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'BillingCity' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'BillingState' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'BillingCountry' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'BillingPostalCode' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Total' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'InvoiceId', 'unique' => 1),
			'IFK_InvoiceCustomerId' => array('column' => 'CustomerId', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'InvoiceId' => 1,
			'CustomerId' => 1,
			'InvoiceDate' => '2015-03-03 15:29:31',
			'BillingAddress' => 'Lorem ipsum dolor sit amet',
			'BillingCity' => 'Lorem ipsum dolor sit amet',
			'BillingState' => 'Lorem ipsum dolor sit amet',
			'BillingCountry' => 'Lorem ipsum dolor sit amet',
			'BillingPostalCode' => 'Lorem ip',
			'Total' => ''
		),
	);

}
