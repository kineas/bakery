<?php
/**
 * EmployeeFixture
 *
 */
class EmployeeFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'employee';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'EmployeeId' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'LastName' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'FirstName' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Title' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'ReportsTo' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'BirthDate' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'HireDate' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'Address' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 70, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'City' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'State' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Country' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 40, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'PostalCode' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Phone' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 24, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Fax' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 24, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'Email' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 60, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'EmployeeId', 'unique' => 1),
			'IFK_EmployeeReportsTo' => array('column' => 'ReportsTo', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'EmployeeId' => 1,
			'LastName' => 'Lorem ipsum dolor ',
			'FirstName' => 'Lorem ipsum dolor ',
			'Title' => 'Lorem ipsum dolor sit amet',
			'ReportsTo' => 1,
			'BirthDate' => '2015-03-03 15:28:49',
			'HireDate' => '2015-03-03 15:28:49',
			'Address' => 'Lorem ipsum dolor sit amet',
			'City' => 'Lorem ipsum dolor sit amet',
			'State' => 'Lorem ipsum dolor sit amet',
			'Country' => 'Lorem ipsum dolor sit amet',
			'PostalCode' => 'Lorem ip',
			'Phone' => 'Lorem ipsum dolor sit ',
			'Fax' => 'Lorem ipsum dolor sit ',
			'Email' => 'Lorem ipsum dolor sit amet'
		),
	);

}
