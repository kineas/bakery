<?php
App::uses('MediatypesController', 'Controller');

/**
 * MediatypesController Test Case
 *
 */
class MediatypesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mediatype'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testExtdata method
 *
 * @return void
 */
	public function testExtdata() {
		$this->markTestIncomplete('testExtdata not implemented.');
	}

/**
 * testExtdataall method
 *
 * @return void
 */
	public function testExtdataall() {
		$this->markTestIncomplete('testExtdataall not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
