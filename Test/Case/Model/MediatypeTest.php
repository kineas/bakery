<?php
App::uses('Mediatype', 'Model');

/**
 * Mediatype Test Case
 *
 */
class MediatypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.mediatype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Mediatype = ClassRegistry::init('Mediatype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Mediatype);

		parent::tearDown();
	}

}
