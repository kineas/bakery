<?php
App::uses('Invoiceline', 'Model');

/**
 * Invoiceline Test Case
 *
 */
class InvoicelineTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.invoiceline'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Invoiceline = ClassRegistry::init('Invoiceline');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Invoiceline);

		parent::tearDown();
	}

}
