<?php
App::uses('Playlisttrack', 'Model');

/**
 * Playlisttrack Test Case
 *
 */
class PlaylisttrackTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.playlisttrack'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Playlisttrack = ClassRegistry::init('Playlisttrack');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Playlisttrack);

		parent::tearDown();
	}

}
