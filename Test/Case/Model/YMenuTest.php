<?php
App::uses('YMenu', 'Model');

/**
 * YMenu Test Case
 *
 */
class YMenuTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.y_menu',
		'app.acos'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->YMenu = ClassRegistry::init('YMenu');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->YMenu);

		parent::tearDown();
	}

}
